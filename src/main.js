import './assets/main.css'
import './assets/tailwind.css'

import * as Vue from 'vue'
import axios from 'axios'
import App from './App.vue'
import i18n from './lang/index'
import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'
import * as ElementPlusIconsVue from '@element-plus/icons-vue'

const app = Vue.createApp(App).use(ElementPlus).use(i18n)
// // 发送GET请求
// axios.get('/api/user')
//   .then(function (response) {
//     console.log(response);
//   })
//   .catch(function (error) {
//     console.log(error);
//   });

// // 发送POST请求
// axios.post('/api/user', {
//     firstName: 'John',
//     lastName: 'Doe'
//   })
//   .then(function (response) {
//     console.log(response);
//   })
//   .catch(function (error) {
//     console.log(error);
//   });

app.config.globalProperties.axios = axios
// app.config.globalProperties.qs = qs

app.mount('#app')

for (const [key, component] of Object.entries(ElementPlusIconsVue)) {
    app.component(key, component)
}
