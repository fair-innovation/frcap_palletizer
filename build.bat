@echo off
chcp 65001
SET buildDir=build
SET packageFile=package.json

for /f "tokens=1,2* delims={}:, " %%a in (package.json) do (
    if %%~a EQU name (
        SET buildName=%%~b
    )
    if %%~a EQU version (
        SET buildVersion=%%~b
    )
)

echo Building %buildName%-v%buildVersion%
SET buildNV=%buildName%-v%buildVersion%

call npm run build

md %buildDir%

if exist %buildNV% (
    SET compressedFileName=%buildNV%.tar.gz
    echo %buildNV% exists.
) else (
    echo Error: %buildNV% does't exist.
    echo built failed.
    echo exit!
    exit /b
)

if exist %packageFile% (
    echo %packageFile% exists.
) else (
    echo %packageFile% does't exist.
    echo built failed.
    echo exit!
    exit /b
)

copy %packageFile% %buildNV%

if exist %compressedFileName% (
    echo %compressedFileName% exists. Del..
    del %compressedFileName%
)
if exist %buildNV%.frcap (
    echo %buildNV%.frcap exists. Del..
    del %buildNV%.frcap
)
tar -czvf %compressedFileName% %buildNV%
ren %compressedFileName% %buildNV%.frcap

move %buildNV%.frcap build

if exist %buildNV% (
    rmdir /Q /S %buildNV%
)

echo %buildNV%.frcap Built Successfully!