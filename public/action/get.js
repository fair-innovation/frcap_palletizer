//var express = require('express');
//var app = express();
//var bodyParser = require('body-parser');
//const events = require('events');
//var multer  = require('multer');
//var fs = require("fs");

var node = "/usr/local/etc/node/sys"
var Socket_Cmd = require(node + '/socket/socket_cmd');
var Socket_Status = require(node + '/socket/socket_status');
var Socket_File = require(node + '/socket/socket_file');
var SqliteDB = require(node + '/sqlite3/sqlite.js').SqliteDB;
var SqliteDB_exec = require(node + '/sqlite3/sqlite.js').SqliteDB_exec;
var SqliteDB_query = require(node + '/sqlite3/sqlite.js').SqliteDB_query;
var Sqlite3_Action = require(node + '/better-sqlite3/better-sqlite3.js');
var File_Handle = require(node + '/tools/file_handle');
var express = require(node + '/tools/express');
var app = express();
var bodyParser = require(node + '/tools/body-parser');
const events = require(node + '/tools/events');
var fs = require(node + '/tools/fs')

var sqlite = new Sqlite3_Action();
var file_handle = new File_Handle();
var DB_POINTS = "/usr/local/etc/web/file/points/web_point.db";
var DIR_PALLETIZING = "/usr/local/etc/web/file/palletizing/";
 
// 创建 application/x-www-form-urlencoded 编码解析
//var urlencodedParser = bodyParser.urlencoded({ extended: false })

function wait(second) {
    // execSync 属于同步方法；异步方式请根据需要自行查询 node.js 的 child_process 相关方法；
    var ChildProcess_ExecSync = require('child_process').execSync;
    ChildProcess_ExecSync('sleep ' + second);
};
 
app.use(bodyParser.urlencoded({ extended: false }));
//app.use(multer({ dest: '/tmp/'}).array('image'));


function isJSON(str) {
    if (typeof str == 'string') {
        try {
            var obj=JSON.parse(str);
            if(typeof obj == 'object' && obj ){
                return true;
            }else{
                return false;
            }

        } catch(e) {
            console.log('error：'+str+'!!!'+e);
            return false;
        }
    }
    console.log('It is not a string!')
}

function get() {
    var socket_cmd = new Socket_Cmd();
    var socket_status = new Socket_Status();
    var socket_file = new Socket_File();
    var event_socket = new events.EventEmitter();
    
    this.handler = function(json_data, res) {
        var cmd = json_data.cmd;
        console.log("json_data = " + json_data);
        var data_json = json_data.data;
        var response_data = {}
        var response_status = 200;
        var recv_data = "";
        
        switch (cmd) {
            case 'get_points':
                var sql = "select * from points order by name ASC";
                var sql_data = sqlite.queryall(DB_POINTS, sql);
                //var name;             
                for (var i = 0; i < sql_data.length; i++) {
                    //name = sql_data[i].name
                    //console.log("name = " + name);
                    response_data[sql_data[i].name] = sql_data[i];
                }
                // 按照 数字升序 + 首字母开头升序 + 中文开头升序 的方式，反馈数据给前端页面进行显示
                event_socket.emit('response', res, response_status, response_data);
                break;
            case 'frcap_palletizer_get_formula_list':
                var files = [];
                var pa = fs.readdirSync(DIR_PALLETIZING);
                pa.forEach(function (ele, index) {
                    if (ele != "palletizing_init.db" && ele != "palletizing_program.db") {
                        files.push(ele);
                    }
                });
                response_data = files;
                event_socket.emit('response', res, response_status, response_data);                    
                break;
            case 'frcap_palletizer_get_formula':
                var palletizing_db = DIR_PALLETIZING + data_json.name + "/palletizing.db";
                console.log("palletizing_db = " + palletizing_db);
                //wait(0.2);
                var sql = "";
                //var sql_data = {};
                sql = "select * from box_cfg order by box_id ASC";
                response_data["box_config"] = sqlite.queryall(palletizing_db, sql);
                   
                sql = "select * from pallet_cfg";
                response_data["pallet_config"] = sqlite.queryget(palletizing_db, sql);
                
                sql = "select * from device_cfg";
                response_data["device_config"] = sqlite.queryget(palletizing_db, sql);
                
                sql = "select * from laminar_separator";          
                response_data["laminar_separator_config"] = sqlite.queryget(palletizing_db, sql);

                sql = "select * from pattern_cfg";
                response_data["pattern_config"] = {};
                response_data["pattern_config"]["patterns"] = sqlite.queryall(palletizing_db, sql);

                sql = "select * from pattern_sequence_cfg";
                response_data["pattern_config"]["flag"] = sqlite.queryget(palletizing_db, sql).flag;
                response_data["pattern_config"]["layers"] = sqlite.queryget(palletizing_db, sql).layers;
                response_data["pattern_config"]["sequence"]= sqlite.queryget(palletizing_db, sql).sequence;
                                              
                sql = "select * from lefttransitionpoint";
                response_data["lefttransitionpoint"] = sqlite.queryget(palletizing_db, sql);

                sql = "select * from righttransitionpoint";
                response_data["righttransitionpoint"] = sqlite.queryget(palletizing_db, sql);
                                          
                sql = "select * from advanced_cfg";
                response_data["advanced_config"] = sqlite.queryget(palletizing_db, sql);
                event_socket.emit('response', res, response_status, response_data);
                break;
            default:
                response_status = 404;
                event_socket.emit('response', res, response_status, "fail");
                break;
        }  
    }
    
    event_socket.on('response', (res, response_status, response_data)=>{
        res.writeHead(response_status);
        if (response_data === "fail") {
            console.log("cmd not found")
            res.end(response_data);
        } else {
            if (Object.prototype.toString.call(response_data) === '[object Object]' || Object.prototype.toString.call(response_data) === '[object Array]') {  
                console.log("isJSON object true");
                console.log("response_data = " + JSON.stringify(response_data));
                res.end(JSON.stringify(response_data));
            } else {
                console.log("isJSON object false");
                console.log("response_data = " + response_data);
                res.end(response_data);
            }
        }
    });
}

module.exports = get;
