import http from "./http";

const act = (config) => {
    return http({
        ...config,
        method: 'POST',
        url: '/action/act',
        data: config
    })
}

const set = (config) => {
    return http({
        ...config,
        method: 'POST',
        url: '/action/set',
        data: config
    })
}

const get = (config) => {
    return http({
        ...config,
        method: 'POST',
        url: '/action/get',
        data: config
    })
}

const upload = (config) => {
    return http({
        ...config,
        method: 'POST',
        url: '/action/upload',
        data: config
    })
}

const download = (config) => {
    return http({
        ...config,
        method: 'GET',
        url: `/action/download?pathfilename=${config}`,
        responseType: 'blob'
    })
}

/**
 * 自定义指令接口
 * @param {String} url 自定义指令URL
 * @param {Object} config 自定义指令参数
 * @returns 
 */
const custom = (url, config) => {
    return http({
      ...config,
      method: 'POST',
      url: url,
      data: config,
    });
  };

export default {
    act,
    set,
    get,
    upload,
    download,
    custom
}