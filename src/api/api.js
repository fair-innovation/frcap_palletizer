import request from "@/utils/request"

// get APIs
/**
 * 获取状态页启停标志位
 * @returns promise
 */
const getStatusPageEnableFlag = () => {
    return request.get({
        cmd: "get_status_flag"
    })
}

/**
 * 获取所有示教点数据
 * @returns promise
 */
const getPoints = () => {
    return request.get({
        cmd: "get_points"
    })
}

/**
 * 获取当前系统变量
 * @returns promise
 */
const getSystemInfo = () => {
    return request.get({
        cmd: "get_syscfg"
    })
}

// act APIs
/**
 * 保存示教点
 * @param {string} name 示教点名称
 * @param {string} toolNum 工具坐标系编号
 * @param {string} workpieceNum 工件坐标系编号
 * @returns promise
 */
const savePoint = (name, toolNum, workpieceNum) => {
    return request.act({
        cmd: "save_point",
        data: {
            "name": name,
            "speed": "100",
            "elbow_speed": "100",
            "acc": "180",
            "elbow_acc": "180",
            "toolnum": toolNum,
            "workpiecenum": workpieceNum,
            "update_allprogramfile": 1
        }
    })
}

/**
 * 设置码垛状态页启停标志位
 * @param {int} flag 0-无状态页，1-扭矩状态页，2-康养状态页，3-码垛状态页
 * @returns promise
 */
const setStatusPageEnableFlag = (flag) => {
    return request.act({
        cmd: "set_status_flag",
        data: {
            page_flag: flag
        }
    })
}

const setExtAxisLoadDriver = () => {
    return request.set({
        cmd: 655,
        data: {
            content: "ExtDevLoadUDPDriver()"
        }
    })
}

/**
 * 回零设置
 * @param {int} mode 
 * @param {int} searchVel 
 * @param {int} latchVel 
 * @returns 
 */
const setAuxServoHoming = (mode, searchVel, latchVel) => {
    return request.set({
        cmd: 290,
        data: {
            content: "ExtAxisSetHoming(1," + mode + "," + searchVel + "," + latchVel + ")"
        }
    })
}

/**
 * 升降轴伺服使能
 * @param {int} enable 0-去使能 1-使能
 * @returns 
 */
const setEaxisServoOn = (enable) => {
    return request.set({
        cmd: 296,
        data: {
            content: "ExtAxisServoOn(1," + enable + ")"
        }
    })
}

/** 升降轴停止*/
const stopEaxisJog = () => {
    return request.set({
        cmd: 240,
        data: {
            content: "StopExtAxisJog"
        }
    })
}

/**
 * 升降轴上升/下降
 * @param {int} enable 0-下降 1-上升
 * @param {int} speed 运行速度
 * @param {int} acc 加速度
 * @param {int} distance 每次升降高度
 * @returns 
 */
const startEaxisJog = (enable,speed,acc,distance) => {
    return request.set({
        cmd: 296,
        data: {
            content: "ExtAxisStartJog(6,1," + enable + "," + speed + "," + acc + "," + distance + ")"
        }
    })
}

export default {
    getStatusPageEnableFlag,
    getPoints,
    getSystemInfo,
    savePoint,
    setStatusPageEnableFlag,
    setExtAxisLoadDriver,
    setAuxServoHoming,
    setEaxisServoOn,
    stopEaxisJog,
    startEaxisJog
}