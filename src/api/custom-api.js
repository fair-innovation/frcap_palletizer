import request from "@/utils/request";
import info from '../../package.json';

let getUrl = `/action/${info.uuid}/get`;
let actUrl = `/action/${info.uuid}/act`;

// get APIs
/**
 * 获取码垛配方名称列表
 * @returns promise
 */
const getRecipeNameList = () => {
    return request.custom(getUrl, {
        cmd: "frcap_palletizer_get_formula_list"
    })
}

/**
 * 获取码垛配方内容
 * @returns promise
 */
const getRecipeContent = (name) => {
    return request.custom(getUrl, {
        cmd: "frcap_palletizer_get_formula",
        data: {
            name: name
        }
    })
}

// act APIs
/**
 * 重命名配方名称
 * @param {string} name old_name 被重命名配方名称
 * @param {string} name new_name 重命名的配方名称
 * @returns 
 */
const renameRecipe = (old_name, new_name) => {
    return request.custom(actUrl, {
        cmd: "frcap_palletizer_rename_formula",
        data: {
            old_name,
            new_name
        }
    })
}

/**
 * 复制配方
 * @param {string} old_name 被复制配方名称
 * @param {string} new_name 复制后命名的配方名称
 * @returns 
 */
const copyRecipe = (old_name, new_name) => {
    return request.custom(actUrl, {
        cmd: "frcap_palletizer_copy_formula",
        data: {
            old_name,
            new_name
        }
    })
}

/**
 * 删除配方
 * @param {string} name 配方名称
 * @returns 
 */
const deleteRecipe = (name) => {
    return request.custom(actUrl, {
        cmd: "frcap_palletizer_delete_formula",
        data: {
            name
        }
    })
}

/**
 * 创建码垛配方
 * @param {string} newName 新的码垛配方名称
 * @returns promise
 */
const addRecipe = (newName) => {
    return request.custom(actUrl, {
        cmd: "frcap_palletizer_create_formula",
        data: {
            name: newName
        }
    })
}

/**
 * 生成码垛程序
 * @param {string} left_palletizing_name 码垛/拆垛程序左配方名称
 * @param {string} right_palletizing_name 码垛/拆垛程序右配方名称
 * @param {string} palletizing_program_name 码垛程序名称
 * @param {string} depalletizing_program_name 拆垛程序名称
 * @returns promise
 */
const generateProgram = (left_palletizing_name, right_palletizing_name, palletizing_program_name, depalletizing_program_name) => {
    return request.custom(actUrl, {
        cmd: "frcap_palletizer_generate_program",
        data: {
            left_palletizing_name,
            right_palletizing_name,
            palletizing_program_name,
            depalletizing_program_name
        }
    })
}

/**
 * 新增箱子
 * @param {string} formulaName 当前编辑的配方名称
 */
const addBox = (formulaName) => {
    return request.custom(actUrl, {
        cmd: "frcap_palletizer_config_box_add",
        data: {
            formula_name: formulaName
        }
    })
}

/**
 * 重命名箱子
 * @param {string} formulaName 当前编辑的配方名称
 * @param {int} boxId 当前编辑的箱子id
 * @param {string} boxName 当前编辑的箱子重命名名称
 */
const renameBox = (formulaName, boxId, boxName) => {
    return request.custom(actUrl, {
        cmd: "frcap_palletizer_config_box_rename",
        data: {
            formula_name: formulaName,
            box_id: boxId,
            name: boxName
        }
    })
}

/**
 * 复制箱子
 * @param {string} formulaName 当前编辑的配方名称 
 * @param {int} boxId 当前编辑的箱子id
 */
const copyBox = (formulaName, boxId) => {
    return request.custom(actUrl, {
        cmd: "frcap_palletizer_config_box_copy",
        data: {
            formula_name: formulaName,
            box_id: boxId
        }
    })
}

/**
 * 删除箱子
 * @param {string} formulaName 当前编辑的配方名称
 * @param {int} boxId 当前编辑的箱子id
 */
const deleteBox = (formulaName, boxId) => {
    return request.custom(actUrl, {
        cmd: "frcap_palletizer_config_box_delete",
        data: {
            formula_name: formulaName,
            box_id: boxId
        }
    })
}

/**
 * 箱子配置
 * @param {string} formulaName 当前编辑的配方名称
 * @param {Object} boxData 当前配置箱子的参数对象
 * boxData：{
 *      box_id：当前编辑的箱子id；
 *      name：当前编辑的箱子名称；
 *      length：箱子长度；
 *      width：箱子宽度；
 *      height：箱子高度；
 *      payload：箱子负载；
 *      tag_enable：工件朝向是否开启；
 *      tag：工件朝向；
 *      grip_point：抓取点名称；
 *      IO：到位信号：[传送带到位信号, 工件到位信号]；
 * }
 * @returns promise
 */
const configBox = (formulaName, boxData) => {
    return request.custom(actUrl, {
        cmd: "frcap_palletizer_config_box",
        data: {
            formula_name: formulaName,
            boxs: boxData
        }
    })
}

/**
 * 托盘配置
 * @param {string} formulaName 当前编辑的配方名称
 * @param {int} palletFront 托盘前边长度
 * @param {int} palletSide 托盘侧边长度
 * @param {int} palletHeight 托盘高度
 * @returns promise
 */
const configPallet = (formulaName, palletFront, palletSide, palletHeight) => {
    return request.custom(actUrl, {
        cmd: "frcap_palletizer_config_pallet",
        data: {
            formula_name: formulaName,
            front: palletFront,
            side: palletSide,
            height: palletHeight
        }
    })
}

/**
 * 新增码垛模式
 * @param {string} formulaName 当前码垛配方名称
 */
const addPattern = (formulaName) => {
    return request.custom(actUrl, {
        cmd: "frcap_palletizer_config_pattern_add",
        data: {
            "formula_name": formulaName
        }
    })
}

/**
 *  重命名码垛模式
 * @param {string} formulaName 当前码垛配方名称
 * @param {int} patternID 当前模式ID
 * @param {string} newPatternName 新的模式名称
 */
const renamePattern = (formulaName, patternID, newPatternName) => {
    return request.custom(actUrl, {
        cmd: "frcap_palletizer_config_pattern_rename",
        data: {
            "formula_name": formulaName,
            "pattern_id": patternID,
            "name": newPatternName
        }
    })
}

/**
 * 复制码垛模式
 * @param {string} formulaName 当前码垛配方名称
 * @param {int} patternID 当前模式ID
 */
const copyPattern = (formulaName, patternID) => {
    return request.custom(actUrl, {
        cmd: "frcap_palletizer_config_pattern_copy",
        data: {
            "formula_name": formulaName,
            "pattern_id": patternID
        }
    })
}

/**
 * 删除码垛模式
 * @param {string} formulaName 当前码垛配方名称
 * @param {int} patternID 当前模式ID
 */
const deletePattern = (formulaName, patternID) => {
    return request.custom(actUrl, {
        cmd: "frcap_palletizer_config_pattern_delete",
        data: {
            "formula_name": formulaName,
            "pattern_id": patternID
        }
    })
}

/**
 * 配置码垛垛型序列
 * @param {string} formulaName 当前码垛配方名称
 * @param {int} layers 垛型层数
 * @param {string} sequence 垛型序列
 * @returns 
 */
const configSequence = (formulaName, layers, sequence) => {
    return request.custom(actUrl, {
        cmd: "frcap_palletizer_config_pattern_sequence",
        data: {
            "formula_name": formulaName,
            "layers": layers,
            "sequence": sequence
        }
    })
}

/**
 * 模式配置
 * @param {string} formulaName 当前编辑的配方名称
 * @param {array} patternArr 模式对象数组
 * @returns promise
 */
const configPattern = (formulaName, patternArr) => {
    return request.custom(actUrl, {
        cmd: "frcap_palletizer_config_pattern",
        data: {
            formula_name: formulaName,
            patterns: patternArr,
        }
    })
}

/**
 * 配置设备尺寸
 * @param {string} formulaName 当前编辑的配方名称
 * @param {int} x 
 * @param {int} y 
 * @param {int} z 
 * @param {int} angle 
 * @returns 
 */
const configDevice = (formulaName, x, y, z, angle) => {
    return request.custom(actUrl, {
        cmd: "frcap_palletizer_config_device",
        data: {
            formula_name: formulaName,
            x: x,
            y: y,
            z: z,
            angle: angle
        }
    })
}

/**
 * 高级配置
 * @param {string} formulaName 当前编辑的配方名称
 * @param {*} height 
 * @param {*} x1 
 * @param {*} y1 
 * @param {*} z1 
 * @param {*} x2 
 * @param {*} y2 
 * @param {*} z2 
 * @param {*} waittime 
 * @param {*} liftingShaft 
 * @param {*} liftLayer 
 * @param {*} UDP_IP 
 * @param {*} UDP_port 
 * @param {*} UDP_period 
 * @returns 
 */
const configAdvanced = (formulaName, height, x1, y1, z1, x2, y2, z2, waittime, liftingShaft, liftLayer, UDP_IP, UDP_port, UDP_period) => {
    return request.custom(actUrl, {
        cmd: "frcap_palletizer_advanced_cfg",
        data: {
            formula_name: formulaName,
            height,
            x1,
            y1,
            z1,
            x2,
            y2,
            z2,
            time: waittime,
            lifting_shart_enable: liftingShaft,
            lifting_shart_layer: liftLayer,
            UDP_IP,
            UDP_port,
            UDP_period
        }
    })
}

/**
 * 码垛层隔板配置
 * @param {string} formulaName 当前编辑的配方名称
 * @param {int} enable 0:不启用,1:启用
 * @param {int} length 长
 * @param {int} width 宽
 * @param {int} height 高
 * @returns 
 */
const setPalletizerLaminar = (formulaName, enable,length,width,height) => {
    return request.custom(actUrl, {
        cmd: "frcap_palletizer_laminar_separator",
        data: {
            formula_name: formulaName,
            enable: enable,
            length: length,
            width: width,
            height: height
        }
    })
}

export default { 
    getRecipeNameList,
    getRecipeContent,
    renameRecipe,
    copyRecipe,
    deleteRecipe,
    addRecipe,
    generateProgram,
    addBox,
    renameBox,
    copyBox,
    deleteBox,
    configBox,
    configPallet,
    addPattern,
    renamePattern,
    copyPattern,
    deletePattern,
    configSequence,
    configPattern,
    configDevice,
    configAdvanced,
    setPalletizerLaminar
}