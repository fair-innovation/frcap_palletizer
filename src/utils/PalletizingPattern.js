import { ElMessage } from "element-plus";

/**
 * 判断传入的对象参数是否为空
 * @param {object} obj 对象参数
 */
function isEmptyObject(obj) {
    if (Object.prototype.toString.call(obj) === '[object Object]') {
        if (Object.keys(obj).length == 0) {
            return true;
        } else {
            return false;
        }
    }
}

/** 码垛模式配置canvas类 */
class PalletizingPattern {
    /**
     * 码垛模式配置构造函数
     * @param {object} pallet 托盘对象
     * @param {array} boxList 箱子对象列表
     * @param {number} selectedRecipeBoxID 已选的配方箱子ID
     * @param {number} baseX 底座X轴
     * @param {number} baseY 底座Y轴
     * @param {number} baseZ 底座Z轴
     * @param {number} baseAngle 底座偏移角度
     * @param {object} refPoint 参考点对象
     * @param {string} patternType 模式类型
     * @param {string} refPatternType 参考模式类型
     * @param {object} refPatternBoxGroup 参考模式原始点位组数据
     * @param {int} canvasWidth canvas宽度（可选）
     * @param {int} canvasHeight canvas高度（可选）
     * @param {object} ctx canvas context（可选）
     */
    constructor(pallet, boxList, selectedRecipeBoxID, baseX, baseY, baseZ, baseAngle, refPoint, patternType, refPatternType, refPatternBoxGroup, canvasWidth, canvasHeight, ctx) {
        this.palletizingJson = JSON.parse(localStorage.getItem('messages'));
        // 创建托盘编辑区域Html元素
        let canvasLabelRight = document.createElement('span');
        let canvasLabelLeft = document.createElement('span');
        canvasLabelRight.innerText = this.palletizingJson._right_pallet_mode + "(Front)";
        canvasLabelLeft.innerText = this.palletizingJson._left_pallet_mode_tips;
        this.canvasWidth = canvasWidth || 400;
        this.canvasHeight = canvasHeight || 350;
        this.patternDiv = document.getElementById('palletizingPattern');
        this.patternCanvas = document.createElement('canvas');
        this.patternCanvas.setAttribute('id', 'patternCanvas');
        this.patternCanvas.setAttribute('width', this.canvasWidth);
        this.patternCanvas.setAttribute('height', this.canvasHeight);
        this.patternCanvas.innerHTML =
        `
            Canvas not supported
        `;
        this.patternDiv.appendChild(this.patternCanvas);
        this.patternDiv.appendChild(canvasLabelRight);
        this.patternDiv.appendChild(canvasLabelLeft);
        this.ctx = ctx || this.patternCanvas.getContext('2d');
        // 托盘模式编辑所需参数
        this.palletFront = pallet.front;
        this.palletSide = pallet.side;
        this.palletHeight = pallet.height;
        this.boxID = selectedRecipeBoxID;
        this.boxList = boxList;
        this.boxLength = boxList.find(el => el.box_id == this.boxID).length;
        this.boxWidth = boxList.find(el => el.box_id == this.boxID).width;
        this.boxHeight = boxList.find(el => el.box_id == this.boxID).height;
        this.boxTagEnable = boxList.find(el => el.box_id == this.boxID).tag_enable;
        this.boxTag = boxList.find(el => el.box_id == this.boxID).tag;
        this.baseX = baseX;
        this.baseY = baseY;
        this.baseZ = baseZ;
        this.baseAngle = baseAngle;
        this.refPoint = refPoint;
        this.boxGapPX = 0;
        this.coe = 0.25;
        this.canvasPalletFront = 0;
        this.canvasPalletSide = 0;
        this.canvasBoxLength = 0;
        this.canvasBoxWidth = 0;
        this.selectedBox = {};
        this.selectedBoxIndex = null;
        this.patternBoxCollision = false;   // 模式存在箱子碰撞
        this.lastAddedBox = {};   // 码垛上一次添加的工件
        this.boxsIDGroup = [];
        this.boxsGroup = [];
        this.didCollideArr = [];
        this.patternALeftOriginMatrix = [];
        this.patternARightOriginMatrix = [];
        this.drawPoint = {x: 0, y: 0};
        this.originPoint = {x: 0, y: 0};
        this.palletColor = "rgb(219, 198, 45)";
        this.boxColor = "#66ccff";
        this.boxTransparency = 1;   // canvas中工件的透明度
        this.isDeletePattern = false; // 码垛第一次点击删除全部按钮的标志
        this.patternType = patternType;
        this.refPatternType = refPatternType;
        this.refPatternBoxGroup = refPatternBoxGroup;
    }

    /**
     * 析构函数
     * 将div#palletizingPattern元素下创建的子元素全部删除，
     * 在使用后，需将创建的实例变量赋值为null，断开变量对实例对象的引用，释放资源。
     * 例如：
     *     // 创建实例
     *     let instance = new palletizingPatternEdit(...);
     *     // 释放实例
     *     instance.destructor();
     *     instance = null;
     */
    destructor() {
        this.patternDiv.innerHTML = "";
    }

    /**
     * 托盘模式编辑区域初始化
     */
    init() {
        this.canvasPalletFront = this.palletFront * this.coe;
        this.canvasPalletSide = this.palletSide * this.coe;
        this.canvasBoxLength = this.boxLength * this.coe;
        this.canvasBoxWidth = this.boxWidth * this.coe;
        this.originPoint.x = (this.canvasWidth - this.canvasPalletFront) / 2;
        this.originPoint.y = (this.canvasHeight - this.canvasPalletSide) / 2;
        this.drawPoint = this.originPoint;
        this.drawPallet(this.originPoint.x, this.originPoint.y, this.canvasPalletFront, this.canvasPalletSide);       //绘制码垛托盘
        this.bindEvent();
        this.drawArrow();
    }

    /**绘制码垛方向箭头 */
    drawArrow() {
        // 绘制竖向箭头
        this.ctx.beginPath();
        this.ctx.moveTo(this.canvasWidth - 10, this.canvasHeight - 130);
        this.ctx.lineTo(this.canvasWidth -10, this.canvasHeight -10);
        this.ctx.stroke();
        // 绘制箭身
        this.ctx.beginPath();
        this.ctx.moveTo(this.canvasWidth - 10, this.canvasHeight - 150);
        this.ctx.lineTo(this.canvasWidth - 5, this.canvasHeight - 130);
        this.ctx.lineTo(this.canvasWidth - 15, this.canvasHeight - 130);
        this.ctx.closePath();
        this.ctx.stroke();

        // 绘制横向箭头
        this.ctx.beginPath();
        this.ctx.moveTo(this.canvasWidth - 140, this.canvasHeight - 10);
        this.ctx.lineTo(this.canvasWidth -10, this.canvasHeight -10);
        this.ctx.stroke();
        // 绘制箭身
        this.ctx.beginPath();
        this.ctx.moveTo(this.canvasWidth - 160, this.canvasHeight - 10);
        this.ctx.lineTo(this.canvasWidth - 140, this.canvasHeight - 15);
        this.ctx.lineTo(this.canvasWidth - 140, this.canvasHeight - 5);
        this.ctx.closePath();
        this.ctx.stroke();

        this.ctx.restore();

        this.ctx.fillStyle = 'red';
        this.ctx.font = '12px Arial';
        this.ctx.fillText(this.palletizingJson._palletizing_direction, this.canvasWidth - 140, this.canvasHeight - 15);
        this.ctx.restore();
    }

    /**
     * 绘制托盘
     * @param {int} x 托盘绘制点x，单位px
     * @param {int} y 托盘绘制点y，单位px
     * @param {int} f 托盘前边长度，单位px
     * @param {int} s 托盘侧边长度，单位px
     */
    drawPallet(x, y, f, s) {
        this.ctx.save();
        this.ctx.beginPath();
        this.ctx.fillStyle = this.palletColor;
        this.ctx.strokeRect(x, y, f, s);
        this.ctx.fillRect(x, y, f, s);
        this.ctx.restore();
    }

    /**
     * 绘制箱子朝向标签
     * @param {Object} box 当前箱子的数据
     * @param {number} towards 箱子的朝向
     */
    rotateBoxDirection(box, towards) {
        switch (towards) {
            case 1:
                // 绘制盒子朝上箭头
                this.ctx.beginPath();
                this.ctx.moveTo(box.x + box.fl/2, box.y + 5);
                this.ctx.lineTo(box.x + 10, box.y + 15);
                this.ctx.lineTo(box.x + box.fl - 10, box.y + 15);
                this.ctx.closePath();
                this.ctx.fillStyle = 'white';
                this.ctx.fill();
                this.ctx.stroke();
                break;
            case 2:
                // 绘制盒子朝右箭头
                this.ctx.beginPath();
                this.ctx.moveTo(box.x + box.fl - 5, box.y + box.sw/2);
                this.ctx.lineTo(box.x + box.fl - 15, box.y + 10);
                this.ctx.lineTo(box.x + box.fl - 15, box.y + box.sw - 10);
                this.ctx.closePath();
                this.ctx.fillStyle = 'white';
                this.ctx.fill();
                this.ctx.stroke();
                break;
            case 3:
                // 绘制盒子朝下箭头
                this.ctx.beginPath();
                this.ctx.moveTo(box.x + box.fl/2, box.y + box.sw - 5);
                this.ctx.lineTo(box.x + 10, box.y + box.sw - 15);
                this.ctx.lineTo(box.x + box.fl - 10, box.y + box.sw - 15);
                this.ctx.closePath();
                this.ctx.fillStyle = 'white';
                this.ctx.fill();
                this.ctx.stroke();
                break;
            case 4:
                // 绘制盒子朝左箭头
                this.ctx.beginPath();
                this.ctx.moveTo(box.x + 5, box.y + box.sw/2);
                this.ctx.lineTo(box.x + 15, box.y + 10);
                this.ctx.lineTo(box.x + 15, box.y + box.sw - 10);
                this.ctx.closePath();
                this.ctx.fillStyle = 'white';
                this.ctx.fill();
                this.ctx.stroke();
                break;
            default:
                break;
        }
    }

    /**
     * 根据旋转角度旋转箱子朝向标签
     * @param {Object} box 当前箱子的数据
     */
    drawBoxDirection(box) {
        let resultTo;
        switch (box.isRotated) {
            case 0:
                this.rotateBoxDirection(box, box.towards);
                break;
            case 1:
                resultTo = (box.towards + 1) % 4;
                this.rotateBoxDirection(box, resultTo == 0 ? 4 : resultTo);
                break;
            case 2:
                resultTo = (box.towards + 2) % 4;
                this.rotateBoxDirection(box, resultTo == 0 ? 4 : resultTo);
                break;
            case 3:
                resultTo = (box.towards + 3) % 4;
                this.rotateBoxDirection(box, resultTo == 0 ? 4 : resultTo);
                break;
            default:
                break;
        }
    }

    /**
     * 绘制工件方块
     * @param {object} box 工件方块绘制点x，单位px
     * @param {string} color 工件方块颜色
     */
    drawBox(box, color) {
        let textX;
        let textY;
        if (box.index > 9) {
            textX = box.x + (box.fl)/2 - 24;
            textY = box.y + (box.sw)/2 + 8;
        } else {
            textX = box.x + (box.fl)/2 - 16;
            textY = box.y + (box.sw)/2 + 8;
        }
        this.ctx.save();
        this.ctx.beginPath();
        this.ctx.fillStyle = color || this.boxColor;
        console.log(this.refPatternType, 'this.refPatternType')
        // 透明度首先考虑是否碰撞和选中，其次再考虑参考模式是否和当前模式相同
        if (color != 'red' && color != 'green') {
            if (box.type == this.refPatternType && this.refPatternType != '') {
                this.ctx.globalAlpha = 1;
            } else {
                this.ctx.globalAlpha = this.boxTransparency;
            }
        } else {
            this.ctx.globalAlpha = 1;
        }
        this.ctx.strokeRect(box.x, box.y, box.fl, box.sw);
        this.ctx.fillRect(box.x, box.y, box.fl, box.sw);
        this.ctx.restore();
        // 设置工件序号，字体为白色（该步骤一定要放在restore()方法之后，不然工件序号会显示在工件背景色下方）
        this.ctx.fillStyle = 'white';
        this.ctx.font = '900 28px Arial';
        this.ctx.fillText(box.type + box.index, textX, textY, box.sw);
        if (box.tagEnable) {
            this.drawBoxDirection(box);
        }
    }

    /**
     * 判断拖动的工件方块是否超出Canvas边界，超出进行修正
     * @param {object} obj 工件方块对象
     */
    judgePosition(obj) {
        obj.x = obj.x < 0 ? 0 : (obj.x + obj.fl) < this.canvasWidth ? obj.x : this.canvasWidth - obj.fl;
        obj.y = obj.y < 0 ? 0 : (obj.y + obj.sw) < this.canvasHeight ? obj.y : this.canvasHeight - obj.sw;
        // obj.x = obj.x < this.originPoint.x ? this.originPoint.x : (obj.x + obj.fl) < this.originPoint.x + this.canvasPalletFront ? obj.x : this.originPoint.x + this.canvasPalletFront - obj.fl;
        // obj.y = obj.y < this.originPoint.y ? this.originPoint.y : (obj.y + obj.sw) < this.originPoint.y + this.canvasPalletSide ? obj.y : this.originPoint.y + this.canvasPalletSide - obj.sw;
    }

    // windowToCanvas(cx, cy) {
    //     let bbox = this.patternCanvas.getBoundingClientRect();
    //     console.log(this.patternCanvas.width, bbox.width, this.patternCanvas.width / bbox.width);
    //     console.log(this.patternCanvas.height, bbox.height, this.patternCanvas.height / bbox.height);
    //     return {
    //       x: cx - bbox.left * (this.patternCanvas.width / bbox.width),
    //       y: cy - bbox.top * (this.patternCanvas.height / bbox.height)
    //     //   x: cx - bbox.left,
    //     //   y: cy - bbox.top
    //     }
    // }

    /**
     * 鼠标按下事件（选中Canvas中工件方块）
     * @param {object} e 事件对象 
     */
    mouseDown(e) {
        e.preventDefault();
        // console.log("mouse down", e);
        this.isMouseDown = true;
        // console.log(this.windowToCanvas(e.clientX, e.clientY));
        // console.log(e.offsetX, e.offsetY);
        // let {x, y} = this.windowToCanvas(e.clientX, e.clientY);
        // console.log(this.boxsGroup);
        // this.boxsGroup.forEach((item, index, arr) => {
        //     let inBoxX = item.x > e.offsetX ? 0 : item.x + item.fl > e.offsetX ? 1 : 0;
        //     let inBoxY = item.y > e.offsetY ? 0 : item.y + item.sw > e.offsetY ? 1 : 0;
        //     if (inBoxX && inBoxY) {
        //         // 选中
        //         this.selectedBox = item;
        //         this.inBoxWidth = e.offsetX - item.x;
        //         this.inBoxHeight = e.offsetY - item.y;
        //         // console.log(this.selectedBox);
        //     }
        // });
        for (let i = this.boxsGroup.length - 1; i >= 0; i--) {
            let inBoxX = this.boxsGroup[i].x > e.offsetX ? 0 : this.boxsGroup[i].x + this.boxsGroup[i].fl > e.offsetX ? 1 : 0;
            let inBoxY = this.boxsGroup[i].y > e.offsetY ? 0 : this.boxsGroup[i].y + this.boxsGroup[i].sw > e.offsetY ? 1 : 0;
            if (inBoxX && inBoxY) {
                // 选中
                this.setBoxID(this.boxsIDGroup[i]);
                this.selectedBox = this.boxsGroup[i];
                this.selectedBoxIndex = i;
                this.inBoxWidth = e.offsetX - this.boxsGroup[i].x;
                this.inBoxHeight = e.offsetY - this.boxsGroup[i].y;
                break;
            } else {
                // 未选中
                this.selectedBox = {};
                this.selectedBoxIndex = null;
                this.inBoxWidth = null;
                this.inBoxHeight = null;
            }
        }
        this.redraw();
    }

    /**
     * 鼠标移动事件
     * @param {object} e 事件对象
     * @returns 
     */
    mouseMove(e) {
        e.preventDefault();
        // console.log("mouse move", e);
        if (this.isMouseDown) {
            // console.log(this.windowToCanvas(e.clientX, e.clientY));
            // let {x, y} = this.windowToCanvas(e.clientX, e.clientY);
            // this.clearPallet();
            // this.ctx.clearRect(0, 0, this.canvasWidth, this.canvasHeight);
            // this.drawPallet(this.originPoint.x, this.originPoint.y, this.canvasPalletFront, this.canvasPalletSide);
            // console.log(this.selectedBox);
            if (! isEmptyObject(this.selectedBox)) {
                this.selectedBox.x = e.offsetX - this.inBoxWidth;
                this.selectedBox.y = e.offsetY - this.inBoxHeight;
                this.judgePosition(this.selectedBox);
                this.detectCollision();
                this.redraw();
            }
        } else {
            return;
        }
    }

    /**
     * 鼠标抬起事件
     * @param {object} e 事件对象
     */
    mouseUp(e) {
        e.preventDefault();
        // console.log("mouse up", e);
        this.isMouseDown = false;
    }

    /**
     * 鼠标移出Canvas事件
     * @param {object} e 事件对象
     */
    mouseOut(e) {
        e.preventDefault();
        // console.log("mouse out", e);
        this.isMouseDown = false;
    }

    /** Canvas绑定事件 */
    bindEvent() {
        this.patternCanvas.addEventListener("mousedown", (e) => this.mouseDown(e));
        this.patternCanvas.addEventListener("mousemove", (e) => this.mouseMove(e));
        this.patternCanvas.addEventListener("mouseup", (e) => this.mouseUp(e));
        this.patternCanvas.addEventListener("mouseout", (e) => this.mouseOut(e));
    }

    /**
     * 添加工件方块（依据已选工件方块）
     */
    addBox() {
        console.log(this.patternType, 'patternType');
        let addFlag = 1;
        let refBox = {};
        let boxItem = {};
        // 是否超限值初始化，默认不超出托盘范围
        let inBounds = {
            inBoundX: 1,
            inBoundY: 1,
        };
        let boxIndex = this.boxsGroup.length + 1;
        // 判断当前是否为第一个工件方块
        if (isEmptyObject(this.lastAddedBox)) {
            // 是第一个方块，将已初始化的托盘绘制点赋值即可
            boxItem.x = this.drawPoint.x;
            boxItem.y = this.drawPoint.y;
            boxItem.fl = this.canvasBoxLength;
            boxItem.sw = this.canvasBoxWidth;
            boxItem.isRotated = 0;
            boxItem.index = boxIndex++;
            boxItem.type = this.patternType;
            boxItem.tagEnable = this.boxTagEnable;
            boxItem.towards = this.boxTag;
        } else {
            let lastBoxHeight = this.boxList.find(el => el.box_id == [...this.boxsIDGroup].pop()).height;
            let currBoxHeight = this.boxList.find(el => el.box_id == this.boxID).height;
            // 判断新增箱子类型是否一致或者高度一致
            if ([...this.boxsIDGroup].pop() == this.boxID || lastBoxHeight == currBoxHeight) {
                // 不是第一个方块，则依据已选工件方块数据，若无已选工件方块则为上一次添加的工件方块
                refBox = isEmptyObject(this.selectedBox) ? this.lastAddedBox : this.selectedBox;
                console.log(refBox, 'refBox');
                // 新工件方块是否会超出托盘边界
                boxItem.x = refBox.x + refBox.fl + this.boxGapPX;
                boxItem.y = refBox.y + refBox.sw + this.boxGapPX;
                inBounds = this.detectOutPalletBounds(boxItem.x, boxItem.y);
                // console.log(inBounds);
                if (inBounds.inBoundX && inBounds.inBoundY) {
                    boxItem.y = refBox.y;
                } else if (!inBounds.inBoundX && inBounds.inBoundY) {
                    boxItem.x = this.originPoint.x;
                } else if (inBounds.inBoundX && !inBounds.inBoundY) {
                    boxItem.y = refBox.y;
                } else {
                    ElMessage({
                        message: this.palletizingJson.warning_messages[6],
                        type: 'warning',
                    })
                }
                if ([...this.boxsIDGroup].pop() == this.boxID) {
                    boxItem.fl = refBox.fl;
                    boxItem.sw = refBox.sw;
                    boxItem.isRotated = refBox.isRotated;
                } else {
                    boxItem.fl = this.canvasBoxLength;
                    boxItem.sw = this.canvasBoxWidth;
                    boxItem.isRotated = 0;
                }
                boxItem.index = boxIndex++;
                boxItem.type = this.patternType;
                boxItem.tagEnable = this.boxTagEnable;
                boxItem.towards = this.boxTag;
            } else {
                addFlag = 0;
            }
        }
        if (addFlag) {
            if (inBounds.inBoundX || inBounds.inBoundY ) {
                // 绘制工件方块
                this.drawBox(boxItem);
                // 存储绘制方块数据
                this.boxsGroup.push(boxItem);
                this.boxsIDGroup.push(this.boxID);
                this.lastAddedBox = boxItem;
                console.log("addbox:" + JSON.stringify(this.lastAddedBox));
                console.log("addbox_boxsGroup:" + JSON.stringify(this.boxsGroup));
            }
            this.detectAllCollision();
            if (this.didCollideArr.length > 0) {
                this.patternBoxCollision = true;
            } else {
                this.patternBoxCollision = false;
            }
            this.redraw();
        }
        return addFlag;
    }

    /** 旋转选中的工件方块对象 */
    rotateBox() {
        if (! isEmptyObject(this.selectedBox)) {
            this.selectedBox.isRotated++;
            if (this.boxLength != this.boxWidth) {
                this.selectedBox.fl = [this.selectedBox.sw, this.selectedBox.sw = this.selectedBox.fl][0];
            }
            this.selectedBox.isRotated = this.selectedBox.isRotated % 4;
            this.detectCollision();
            this.redraw();
        } else {
            ElMessage({
                message: this.palletizingJson.warning_messages[7],
                type: 'warning',
            })
        }
    }

    /** 删除选中的工件方块对象 */
    deletebox() {
        if (this.selectedBoxIndex != null) {
            this.boxsGroup.splice(this.selectedBoxIndex, 1);
            this.boxsIDGroup.splice(this.selectedBoxIndex, 1);
            if (this.didCollideArr.length) {
                this.didCollideArr = [];
                this.detectAllCollision();
            }
            if (this.didCollideArr.length > 0) {
                this.patternBoxCollision = true;
            } else {
                this.patternBoxCollision = false;
            }
            // 如果选择的方块是最后一个，则上次已添加的方块向前移一位
            if (this.selectedBoxIndex == this.boxsGroup.length) {
                this.lastAddedBox = this.boxsGroup[this.selectedBoxIndex - 1];
                console.log(this.lastAddedBox);
            } 
            // 如果删除的是最后一个方块，则上次清空上次已添加的方块对象变量
            if (!this.boxsGroup.length) {
                this.lastAddedBox = {};
            }
            // 清空已选方块对象变量
            this.selectedBox = {};
            this.selectedBoxIndex = null;
            this.inBoxWidth = null;
            this.inBoxHeight = null;
            this.redraw();
        } else {
            ElMessage({
                message: this.palletizingJson.warning_messages[7],
                type: 'warning',
            })
        }
    }

    /**
     * 选中工件方块后输入行列执行批量添加
     * @param {int} rows 
     * @param {int} columns 
     */
    addBoxsInBatches(rows, columns) {
        if (! isEmptyObject(this.selectedBox)) {
            for (let i = 0; i < rows; i++) {
                for (let j = 0; j < columns; j++) {
                    if (i || j) {
                        let newBox = {};
                        newBox.x = this.selectedBox.x + this.selectedBox.fl * j;
                        newBox.y = this.selectedBox.y + this.selectedBox.sw * i;
                        newBox.fl = this.selectedBox.fl;
                        newBox.sw = this.selectedBox.sw;
                        newBox.isRotated = this.selectedBox.isRotated;
                        newBox.type = this.patternType;
                        this.boxsGroup.push(newBox);
                        this.boxsIDGroup.push(this.boxID);
                    }
                }
            }
            // 批量添加时也需要验证碰撞检测
            this.detectAllCollision();
            if (this.didCollideArr.length > 0) {
                this.patternBoxCollision = true;
            } else {
                this.patternBoxCollision = false;
            }
            console.log("boxsGroup:" + JSON.stringify(this.boxsGroup));
            this.redraw();
        } else {
            ElMessage({
                message: this.palletizingJson.warning_messages[7],
                type: 'warning',
            })
        }
    }

    /**
     * 检测新增的工件方块是否会超出托盘边界
     * @param {int} x 新工件方块绘制点x
     * @param {int} y 新工件方块绘制点y
     */
    detectOutPalletBounds(x, y) {
        let inBoundX = (x - this.originPoint.x + this.canvasBoxLength) <= this.canvasPalletFront ? 1 : 0;
        let inBoundY = (y - this.originPoint.y + this.canvasBoxWidth) <= this.canvasPalletSide ? 1 : 0;
        return {
            inBoundX: inBoundX,
            inBoundY: inBoundY
        };
    }

    /** 方块之间的碰撞检测--遍历所有方块 */
    detectAllCollision() {
        this.boxsGroup.forEach((item, itemIndex) => {
            this.boxsGroup.forEach((element, elementIndex) => {
                if (itemIndex != elementIndex && item.type == element.type) {
                    let hor = element.x + element.fl > item.x && element.x < item.x + item.fl;
                    let ver = element.y < item.y + item.sw && element.y + element.sw > item.y;
                    if (hor && ver) {
                        if (this.didCollideArr.every(didItem => didItem != elementIndex)) {
                            this.didCollideArr.push(elementIndex);
                            if (this.didCollideArr.indexOf(itemIndex) == -1) {
                                this.didCollideArr.push(itemIndex);
                            }
                        }
                    }
                }
            })
        })
    }

    /** 方块之间的碰撞检测--当前选中 */
    detectCollision() {
        this.boxsGroup.forEach((item, index) => {
            if (this.selectedBoxIndex != index && item.type == this.selectedBox.type) {
                let hor = item.x + item.fl > this.selectedBox.x && item.x < this.selectedBox.x + this.selectedBox.fl;
                let ver = item.y < this.selectedBox.y + this.selectedBox.sw && item.y + item.sw > this.selectedBox.y;
                if (hor && ver) {
                    if (this.didCollideArr.every(item => item != index)) {
                        this.didCollideArr.push(index);
                        if (this.didCollideArr.indexOf(this.selectedBoxIndex) == -1) {
                            this.didCollideArr.push(this.selectedBoxIndex);
                        }
                    }
                } else {
                    if (this.didCollideArr.indexOf(index) != -1) {
                        this.didCollideArr.splice(this.didCollideArr.indexOf(index), 1);
                        if (this.didCollideArr.length == 1) {
                            this.didCollideArr.splice(this.didCollideArr.indexOf(this.selectedBoxIndex), 1);
                        }
                    }
                }
            }
        });
        this.detectAllCollision();
        if (this.didCollideArr.length > 0) {
            this.patternBoxCollision = true;
        } else {
            this.patternBoxCollision = false;
        }
    }

    /**
     * 计算右工件盒子中心点，单位：mm   400 790 1070
     * @param {Object} boxItem 右工件方块对象
     */
    calculateRightBoxCenterPoint(boxItem) {
        let rightCenterPoint = {};
        rightCenterPoint.cx = ((boxItem.fl / 2 + boxItem.x) - this.originPoint.x) / this.coe;
        rightCenterPoint.cy = ((boxItem.sw / 2 + boxItem.y) - this.originPoint.y) / this.coe;
        rightCenterPoint.cz = this.boxHeight;
        return rightCenterPoint;
    }

    /**
     * 计算左工件盒子中心点，单位：mm   400 790 1070
     * @param {Object} boxItem 左工件方块对象
     */
    calculateLeftBoxCenterPoint(boxItem) {
        let leftCenterPoint = {};
        leftCenterPoint.cx = ((this.originPoint.x + this.canvasPalletFront) - (boxItem.fl / 2 + boxItem.x)) / this.coe;
        leftCenterPoint.cy = ((boxItem.sw / 2 + boxItem.y) - this.originPoint.y) / this.coe;
        leftCenterPoint.cz = this.boxHeight;
        return leftCenterPoint;
    }

    /**
     * 工件方块按距离排序（冒泡排序）
     * @param {array} arrD 工件方块中心点到原点距离数组
     * @param {array} arr 工件中心点数组
     * @returns arr 返回依据距离排序后的工件中心点数组
     */
    bobbleSort(arrD, arr, boxs) {
        for (let i = 0; i < arrD.length; i++) {
            for (let j = 0; j < arrD.length - 1 - i; j++) {
                if (arrD[j] < arrD[j+1]) {
                    arrD[j] = [arrD[j+1], arrD[j+1] = arrD[j]][0];
                    arr[j] = [arr[j+1], arr[j+1] = arr[j]][0];
                    if (boxs != undefined) {
                        boxs[j] = [boxs[j+1], boxs[j+1] = boxs[j]][0];
                    }
                }
            }
        }

        return arr;
    }

    /**
     * 根据右工位canvas镜像计算左工位方块的原始点位数据
     * @param {array} rightArray 右工位原始点位数据
     * @returns 
     */
    createPatternLeftPoint(rightArray) {
        console.log(rightArray, 'rightArray');
        let leftArray = [];
        rightArray.forEach(item => {
            leftArray.push({
                fl: item.fl,
                index: item.index,
                isRotated: item.isRotated,
                sw: item.sw,
                type: item.type,
                x: this.canvasWidth/2 + (this.canvasWidth/2 - item.x) -item.fl,
                y: item.y
            })
        });
        return leftArray;
    }

    /**
     * 计算所有的右工件方块的放置点坐标
     * @returns ppMatrix
     */
    createPatternRightPointsMatrix(patternData) {
        let ppRightMatrix = {},
            tempRightMatrix = [],
            tempRightDistance = [];
        // let diff_X = Math.round((-790)*Math.cos(Math.PI/180*(-30)) - (-400)*Math.sin(Math.PI/180*(-30)));
        // let diff_Y = Math.round((-790)*Math.sin(Math.PI/180*(-30)) + (-400)*Math.cos(Math.PI/180*(-30)));
            // diff_X = Math.round((-790)*Math.cos(Math.PI/180*60) + (400)*Math.sin(Math.PI/180*60)),
            // diff_Y = Math.round((-790)*Math.cos(Math.PI/180*60) - (400)*Math.sin(Math.PI/180*60));
        // console.log(diff_X, diff_Y);
        this.rightBoxsGroup = [];
        this.patternARightOriginMatrix = [];
        patternData.forEach(item => {
            let rightPointArr = [],
                tempRightPointArr = [];
            let rcp = this.calculateRightBoxCenterPoint(item);

            // 创建码垛右工位矩阵点位
            rightPointArr.push(rcp.cx);                                   // x--工件方块中心点x
            rightPointArr.push(rcp.cy);                                   // y--工件方块中心点y
            rightPointArr.push(rcp.cz);                                   // z--工件方块中心点z（托盘高度+工件高度）
            rightPointArr.push(parseFloat(this.refPoint.rx));           // rx--暂时使用参考点的rx
            rightPointArr.push(parseFloat(this.refPoint.ry));           // ry--暂时使用参考点的ry
            tempRightPointArr.push(rcp.cx);                               // x--工件方块中心点x
            tempRightPointArr.push(rcp.cy);                               // y--工件方块中心点y
            tempRightPointArr.push(rcp.cz);                               // z--工件方块中心点z（托盘高度+工件高度）
            tempRightPointArr.push(parseFloat(this.refPoint.rx));       // rx--暂时使用参考点的rx
            tempRightPointArr.push(parseFloat(this.refPoint.ry));       // ry--暂时使用参考点的ry
            // 如果工件方块进行了旋转，则进行rz减去90度（工具坐标系）
            switch (item.isRotated) {
                case 0:
                    rightPointArr.push(parseFloat(this.refPoint.rz));               // rz--暂时使用参考点的rz
                    tempRightPointArr.push(parseFloat(this.refPoint.rz));           // rz--暂时使用参考点的rz
                    break;
                case 1:
                    if (parseFloat(this.refPoint.rz) >= 0) {
                        rightPointArr.push(parseFloat(this.refPoint.rz) - 90);
                        tempRightPointArr.push(parseFloat(this.refPoint.rz) - 90);
                    } else {
                        rightPointArr.push(parseFloat(this.refPoint.rz) + 90);
                        tempRightPointArr.push(parseFloat(this.refPoint.rz) + 90);
                    }
                    break;
                case 2:
                    if (parseFloat(this.refPoint.rz) >= 0) {
                        rightPointArr.push(parseFloat(this.refPoint.rz) - 180);
                        tempRightPointArr.push(parseFloat(this.refPoint.rz) - 180);
                    } else {
                        rightPointArr.push(parseFloat(this.refPoint.rz) + 180);
                        tempRightPointArr.push(parseFloat(this.refPoint.rz) + 180);
                    }
                    break;
                case 3:
                    if (parseFloat(this.refPoint.rz) >= 90) {
                        rightPointArr.push(parseFloat(this.refPoint.rz) - 270);
                        tempRightPointArr.push(parseFloat(this.refPoint.rz) - 270);
                    } else {
                        rightPointArr.push(parseFloat(this.refPoint.rz) + 90);
                        tempRightPointArr.push(parseFloat(this.refPoint.rz) + 90);
                    }
                    break;
                default:
                    break;
            }
            this.patternARightOriginMatrix.push(rightPointArr);
            tempRightDistance.push(Math.sqrt(Math.pow(rightPointArr[0], 2) + Math.pow(rightPointArr[1], 2)));
            
            let tempRightX = Math.round((tempRightPointArr[1] - Number(this.baseX))*Math.cos(Math.PI/180*(Number(this.baseAngle))) - (tempRightPointArr[0] + Number(this.baseY))*Math.sin(Math.PI/180*(Number(this.baseAngle))));
            let tempRightY = Math.round((tempRightPointArr[1] - Number(this.baseX))*Math.sin(Math.PI/180*(Number(this.baseAngle))) + (tempRightPointArr[0] + Number(this.baseY))*Math.cos(Math.PI/180*(Number(this.baseAngle))));
            // let tempRightX = -790 + tempRightPointArr[1];
            // let tempRightY = 400 + tempRightPointArr[0];
            tempRightPointArr[0] = tempRightX;
            tempRightPointArr[1] = tempRightY;
            tempRightPointArr[2] = tempRightPointArr[2] - Number(this.baseZ);
            tempRightMatrix.push(tempRightPointArr);

            this.rightBoxsGroup.push(item);
        });
        ppRightMatrix["1"] = tempRightMatrix;
        // 右工位点位排序
        // ppRightMatrix["1"] = this.bobbleSort(tempRightDistance, tempRightMatrix, this.rightBoxsGroup);
        console.log(JSON.stringify(this.rightBoxsGroup));
        console.log(JSON.stringify(this.patternARightOriginMatrix));

        return ppRightMatrix;
    }

    /**
     * 计算所有的左工件方块的放置点坐标
     * @returns ppMatrix
     */
    createPatternLeftPointsMatrix(patternData) {
        let ppLeftMatrix = {},
            tempLeftMatrix = [],
            tempLeftDistance = [];
        // let diff_X = Math.round((-790)*Math.cos(Math.PI/180*(-30)) - (-400)*Math.sin(Math.PI/180*(-30)));
        // let diff_Y = Math.round((-790)*Math.sin(Math.PI/180*(-30)) + (-400)*Math.cos(Math.PI/180*(-30)));
            // diff_X = Math.round((-790)*Math.cos(Math.PI/180*60) + (400)*Math.sin(Math.PI/180*60)),
            // diff_Y = Math.round((-790)*Math.cos(Math.PI/180*60) - (400)*Math.sin(Math.PI/180*60));
        // console.log(diff_X, diff_Y);
        this.leftBoxsGroup = [];
        this.patternALeftOriginMatrix = [];
        patternData.forEach(item => {
            let leftPointArr = [],
                tempLeftPointArr = [];
            let lcp = this.calculateLeftBoxCenterPoint(item);
            console.log(lcp, 'lcp');
            // 创建码垛左工位矩阵点位
            leftPointArr.push(lcp.cx);                                   // x--工件方块中心点x
            leftPointArr.push(lcp.cy);                                   // y--工件方块中心点y
            leftPointArr.push(lcp.cz);                                   // z--工件方块中心点z（托盘高度+工件高度）
            leftPointArr.push(parseFloat(this.refPoint.rx));           // rx--暂时使用参考点的rx
            leftPointArr.push(parseFloat(this.refPoint.ry));           // ry--暂时使用参考点的ry
            tempLeftPointArr.push(lcp.cx);                               // x--工件方块中心点x
            tempLeftPointArr.push(lcp.cy);                               // y--工件方块中心点y
            tempLeftPointArr.push(lcp.cz);                               // z--工件方块中心点z（托盘高度+工件高度）
            tempLeftPointArr.push(parseFloat(this.refPoint.rx));       // rx--暂时使用参考点的rx
            tempLeftPointArr.push(parseFloat(this.refPoint.ry));       // ry--暂时使用参考点的ry
            // 如果工件方块进行了旋转，则进行rz减去90度（工具坐标系）
            switch (item.isRotated) {
                case 0:
                    leftPointArr.push(parseFloat(this.refPoint.rz));               // rz--暂时使用参考点的rz
                    tempLeftPointArr.push(parseFloat(this.refPoint.rz));           // rz--暂时使用参考点的rz
                    break;
                case 1:
                    if (parseFloat(this.refPoint.rz) >= 90) {
                        leftPointArr.push(parseFloat(this.refPoint.rz) - 270);
                        tempLeftPointArr.push(parseFloat(this.refPoint.rz) - 270);
                    } else {
                        leftPointArr.push(parseFloat(this.refPoint.rz) + 90);
                        tempLeftPointArr.push(parseFloat(this.refPoint.rz) + 90);
                    }
                    break;
                case 2:
                    if (parseFloat(this.refPoint.rz) >= 0) {
                        leftPointArr.push(parseFloat(this.refPoint.rz) - 180);
                        tempLeftPointArr.push(parseFloat(this.refPoint.rz) - 180);
                    } else {
                        leftPointArr.push(parseFloat(this.refPoint.rz) + 180);
                        tempLeftPointArr.push(parseFloat(this.refPoint.rz) + 180);
                    }
                    break;
                case 3:
                    if (parseFloat(this.refPoint.rz) >= 0) {
                        leftPointArr.push(parseFloat(this.refPoint.rz) - 90);
                        tempLeftPointArr.push(parseFloat(this.refPoint.rz) - 90);
                    } else {
                        leftPointArr.push(parseFloat(this.refPoint.rz) + 90);
                        tempLeftPointArr.push(parseFloat(this.refPoint.rz) + 90);
                    }
                    break;
                default:
                    break;
            }
            // if (item.isRotated) {                                   
            //     if (parseFloat(this.refPoint.rz) >= 0) {
            //         leftPointArr.push(parseFloat(this.refPoint.rz) + 90);          // rz--暂时使用参考点的rz
            //         tempLeftPointArr.push(parseFloat(this.refPoint.rz) + 90);      // rz--暂时使用参考点的rz
            //     } else {
            //         leftPointArr.push(parseFloat(this.refPoint.rz) - 90);          // rz--暂时使用参考点的rz
            //         tempLeftPointArr.push(parseFloat(this.refPoint.rz) - 90);      // rz--暂时使用参考点的rz
            //     }
            // } else {
            //     leftPointArr.push(parseFloat(this.refPoint.rz));               // rz--暂时使用参考点的rz
            //     tempLeftPointArr.push(parseFloat(this.refPoint.rz));           // rz--暂时使用参考点的rz
            // }

            this.patternALeftOriginMatrix.push(leftPointArr);
            tempLeftDistance.push(Math.sqrt(Math.pow(leftPointArr[0], 2) + Math.pow(leftPointArr[1], 2)));
            
            let tempLeftX = Math.round((tempLeftPointArr[1] - Number(this.baseX))*Math.cos(Math.PI/180*(Number(this.baseAngle))) - (-tempLeftPointArr[0] - Number(this.baseY))*Math.sin(Math.PI/180*(Number(this.baseAngle))));
            let tempLeftY = Math.round((tempLeftPointArr[1] - Number(this.baseX))*Math.sin(Math.PI/180*(Number(this.baseAngle))) + (-tempLeftPointArr[0] - Number(this.baseY))*Math.cos(Math.PI/180*(Number(this.baseAngle))));
            // let tempLeftX = -790 + tempLeftPointArr[1];
            // let tempLeftY = -400 - tempLeftPointArr[0];
            tempLeftPointArr[0] = tempLeftX;
            tempLeftPointArr[1] = tempLeftY;
            tempLeftPointArr[2] = tempLeftPointArr[2] - Number(this.baseZ);
            tempLeftMatrix.push(tempLeftPointArr);

            this.leftBoxsGroup.push(item);
        });
        ppLeftMatrix["1"] = tempLeftMatrix;
        // 左工位点位排序
        // ppLeftMatrix["1"] = this.bobbleSort(tempLeftDistance, tempLeftMatrix, this.leftBoxsGroup);
        console.log(JSON.stringify(this.leftBoxsGroup));
        console.log(JSON.stringify(this.patternALeftOriginMatrix));

        return ppLeftMatrix;
    }

    /**
     * 点位矩阵旋转变换
     * @param {object} ppMatrix 模式A点位矩阵
     * @param {int} angle 矩阵变换角度 
     * @returns rppRightMatrix
     */
    rotatePatternPointsMatrix(angle) {
        let rppLeftMatrix = {},
            tempRLeftDistance = [],
            tempRLeftMatrix = [],
            rppRightMatrix = {},
            tempRRightDistance = [],
            tempRRightMatrix = [];
        // 计算码垛模式A旋转中心点
        let leftCenterPoint = {
            x: (this.canvasPalletFront - (((this.leftBoxsGroup[this.leftBoxsGroup.length-1].x + this.leftBoxsGroup[this.leftBoxsGroup.length-1].fl) - this.leftBoxsGroup[0].x) / 2)) / this.coe, 
            y: (((this.leftBoxsGroup[0].y + this.leftBoxsGroup[0].sw) - this.leftBoxsGroup[this.leftBoxsGroup.length-1].y) / 2) / this.coe
        };
        console.log(leftCenterPoint);
        console.log(-Number(this.baseX) + leftCenterPoint.y);
        console.log(-Number(this.baseY) - leftCenterPoint.x);
        
        // let rightCenterPoint = {
        //     x: (((this.boxs[this.boxs.length-1].x + this.boxs[this.boxs.length-1].fl) - this.boxs[0].x) / 2) / this.coe, 
        //     y: (((this.boxs[0].y + this.boxs[0].sw) - this.boxs[this.boxs.length-1].y) / 2) / this.coe
        // };
        let rightCenterPoint = {
            x: (this.rightBoxsGroup[0].x + this.rightBoxsGroup[0].fl - this.rightBoxsGroup[this.rightBoxsGroup.length-1].x) / 2 / this.coe, 
            y: (this.rightBoxsGroup[0].y + this.rightBoxsGroup[0].sw - this.rightBoxsGroup[this.rightBoxsGroup.length-1].y) / 2 / this.coe
        };
        console.log(rightCenterPoint);
        console.log(-Number(this.baseX) + rightCenterPoint.y);
        console.log(-Number(this.baseY) + rightCenterPoint.x);
        // 码垛模式A左工位原始点位依次按旋转点旋转
        this.patternALeftOriginMatrix.forEach(item => {
            let a = [];
            a[0] = Math.round((item[0] - leftCenterPoint.x)*Math.cos(Math.PI/180*angle) - (item[1] - leftCenterPoint.y)*Math.sin(Math.PI/180*angle) + leftCenterPoint.x);
            a[1] = Math.round((item[0] - leftCenterPoint.x)*Math.sin(Math.PI/180*angle) + (item[1] - leftCenterPoint.y)*Math.cos(Math.PI/180*angle) + leftCenterPoint.y);
            a[2] = item[2];
            a[3] = item[3];
            a[4] = item[4];
            a[5] = item[5];
            // 点位旋转后距离计算
            tempRLeftDistance.push(Math.sqrt(Math.pow(a[0], 2) + Math.pow(a[1], 2)));
            // 旋转后点位依据实际基坐标系偏移进行矩阵变换
            let tempLeftX = Math.round((a[1] - Number(this.baseX))*Math.cos(Math.PI/180*(Number(this.baseAngle))) - (-a[0] - Number(this.baseY))*Math.sin(Math.PI/180*(Number(this.baseAngle))));
            let tempLeftY = Math.round((a[1] - Number(this.baseX))*Math.sin(Math.PI/180*(Number(this.baseAngle))) + (-a[0] - Number(this.baseY))*Math.cos(Math.PI/180*(Number(this.baseAngle))));
            // let tempLeftX = -790 + a[1];
            // let tempLeftY = -400 - a[0];
            a[0] = tempLeftX;
            a[1] = tempLeftY;
            a[2] = a[2] - Number(this.baseZ);
            tempRLeftMatrix.push(a);
        });
        // 码垛模式A右工位原始点位依次按旋转点旋转
        this.patternARightOriginMatrix.forEach(item => {
            let b = [];
            b[0] = Math.round((item[0] - rightCenterPoint.x)*Math.cos(Math.PI/180*angle) - (item[1] - rightCenterPoint.y)*Math.sin(Math.PI/180*angle) + rightCenterPoint.x);
            b[1] = Math.round((item[0] - rightCenterPoint.x)*Math.sin(Math.PI/180*angle) + (item[1] - rightCenterPoint.y)*Math.cos(Math.PI/180*angle) + rightCenterPoint.y);
            b[2] = item[2];
            b[3] = item[3];
            b[4] = item[4];
            b[5] = item[5];
            // 点位旋转后距离计算
            tempRRightDistance.push(Math.sqrt(Math.pow(b[0], 2) + Math.pow(b[1], 2)));
            // 旋转后点位依据实际基坐标系偏移进行矩阵变换
            let tempRightX = Math.round((b[1] - Number(this.baseX))*Math.cos(Math.PI/180*(Number(this.baseAngle))) - (b[0] + Number(this.baseY))*Math.sin(Math.PI/180*(Number(this.baseAngle))));
            let tempRightY = Math.round((b[1] - Number(this.baseX))*Math.sin(Math.PI/180*(Number(this.baseAngle))) + (b[0] + Number(this.baseY))*Math.cos(Math.PI/180*(Number(this.baseAngle))));
            // let tempRightX = -790 + b[1];
            // let tempRightY = 400 + b[0];
            b[0] = tempRightX;
            b[1] = tempRightY;
            b[2] = b[2] - Number(this.baseZ);
            tempRRightMatrix.push(b);
        });
        // 旋转变换后的点位矩阵依据距托盘原点排序离排序
        rppLeftMatrix["1"] = this.bobbleSort(tempRLeftDistance, tempRLeftMatrix);
        rppRightMatrix["1"] = this.bobbleSort(tempRRightDistance, tempRRightMatrix);

        return [rppLeftMatrix, rppRightMatrix];
    }

    /**
     * 清空托盘，删除全部的工件方块，并重新初始化
     */
    clearPallet() {
        if (this.boxsGroup.length) {
            if (this.isDeletePattern) {
                this.ctx.save();
                this.ctx.clearRect(0, 0, this.canvasWidth, this.canvasHeight);
                this.ctx.restore();
                this.selectedBox = {};
                this.selectedBoxIndex = null;
                this.inBoxWidth = null;
                this.inBoxHeight = null;
                this.lastAddedBox = {};
                this.boxsGroup = [];
                this.boxsIDGroup = [];
                this.didCollideArr = [];
                this.init();
                this.isDeletePattern = false;
            } else {
                this.isDeletePattern = true;
                ElMessage({
                    message: this.palletizingJson.warning_messages[4],
                    type: 'warning',
                })
            }
        } else {
            ElMessage({
                message: this.palletizingJson.warning_messages[5],
                type: 'warning',
            })
        }
    }

    /**
     * 依据修改后的BoxsGroup重新绘制 
     * @param {int} isClearSelected 是否清空已选方块
     */
    redraw(isClearSelected) {
        if (isClearSelected) {
            // 清空已选方块对象变量
            this.selectedBox = {};
            this.selectedBoxIndex = null;
            this.inBoxWidth = null;
            this.inBoxHeight = null;
        }
        // 清空画布
        this.ctx.save();
        this.ctx.clearRect(0, 0, this.canvasWidth, this.canvasHeight);
        this.ctx.restore();
        // 重绘托盘
        this.drawPallet(this.originPoint.x, this.originPoint.y, this.canvasPalletFront, this.canvasPalletSide);
        if (this.refPatternType != '' && this.refPatternBoxGroup.length != 0) {
            for (let i = 0; i < this.refPatternBoxGroup.length; i++) {
                this.drawBox(this.refPatternBoxGroup[i], '#005e8c');
                this.ctx.restore();
            }
        }
        for (let i = 0; i < this.boxsGroup.length; i++) {
            this.boxsGroup[i].index = i + 1;
            // 重绘工件方块
            if (this.didCollideArr.includes(i)) {
                this.drawBox(this.boxsGroup[i], 'red');     // 碰撞检测的绘制成红色方块
            } else if (this.selectedBoxIndex == i) {
                this.drawBox(this.boxsGroup[i], 'green');   // 选中的绘制成绿色方块
            } else {
                this.drawBox(this.boxsGroup[i]);
            }
        }
        this.drawArrow();
    }

    /**
     * 计算工件像素间隔对应的实际间隔
     * @param {int} boxGapPX 工件的像素间隔
     * @returns boxGapMM
     */
    calculateActualGap(boxGapPX) {
        return boxGapPX / this.coe;
    }

    /**
     * 修改对象中的像素间隔值
     * @param {int} boxGapPX 工件的像素间隔
     */
    changeBoxGap(boxGapPX) {
        this.boxGapPX = boxGapPX;
    }

    /**
     * 获取码垛模板像素点位
     * @returns 
     */
    getBoxsGroup() {
        return this.boxsGroup;
    }

    /**
     * 设置码垛模板像素点位
     */
    setBoxsGroup(data) {
        // 当修改箱子标签朝向后，再次进入模式配置时，同步更新箱子标签朝向
        this.boxsIDGroup.forEach((item, index) => {
            if (this.boxList.find(element => element.box_id == item)) {
                data[index].tagEnable = this.boxList.find(element => element.box_id == item).tag_enable;
            }
        });
        this.boxsGroup = data;
        this.detectCollision();
        this.redraw(1);
    }

    /**
     * 获取箱子类型ID数组
     * @returns 
     */
    getBoxsIDGroup() {
        return this.boxsIDGroup;
    }

    /**
     * 设置箱子类型ID数组
     */
    setBoxsIDGroup(data) {
        this.boxsIDGroup = data;
    }

    /**
     * 设置参考模式箱子透明度
     * @param {int} transp 透明度20~100
     */
    setRefPatternTransparency(transp) {
        this.boxTransparency = transp / 100;
        this.redraw();
    }

    /** 返回当前模式是否存在碰撞 */
    getPatternBoxCollision() {
        return this.patternBoxCollision;
    }

    /**
     * 处理上一次添加的箱子参数
     * @param {object} obj 处理参数
     */
    handleLastAddedBox(obj) {
        this.lastAddedBox = obj;
    }

    /**
     * 设置当前的模式类型
     * @param {string} patternType 模式类型
     */
    setCurrPatternType(patternType) {
        this.patternType = patternType;
    }

    /**
     * 设置参考模式数据
     * @param {string} refPatternType 参考模式类型
     * @param {array} refPatternBoxGroup 参考模式类型虚拟箱子组点位数据
     */
    setRefPattern(refPatternType, refPatternBoxGroup) {
        this.refPatternType = refPatternType;
        this.refPatternBoxGroup = refPatternBoxGroup;
        this.redraw(1);
    }

    /**
     * 设置箱子ID及箱子属性参数
     * @param {number} boxID 已选的配方箱子ID
     */
    setBoxID(boxID) {
        this.boxID = boxID;
        this.boxLength = this.boxList.find(el => el.box_id == this.boxID).length;
        this.boxWidth = this.boxList.find(el => el.box_id == this.boxID).width;
        this.boxHeight = this.boxList.find(el => el.box_id == this.boxID).height;
        this.boxTagEnable = this.boxList.find(el => el.box_id == this.boxID).tag_enable;
        this.boxTag = this.boxList.find(el => el.box_id == this.boxID).tag;
        this.canvasBoxLength = this.boxLength * this.coe;
        this.canvasBoxWidth = this.boxWidth * this.coe;
        // 切换箱子时，清空画布区域已经选中的工件
        this.selectedBox = {};
        this.selectedBoxIndex = null;
        this.redraw();
    }

    /**
     * 获取选中箱子的boxid属性
     * @returns boxID
     */
    getBoxID() {
        return this.boxID;
    }
}

export default PalletizingPattern