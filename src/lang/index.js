import { createI18n } from 'vue-i18n'
// 从语言包文件中导入语言包对象
import zh from './zh'
import en from './en'
const messages = {
  zh: {
    ...zh
  },
  en: {
    ...en
  }
}
const i18n = new createI18n({
  locale: 'zh',
  messages,
  silentFallbackWarn: true, // 抑制警告
  globalInjection: true // 全局注入
})
export default i18n
