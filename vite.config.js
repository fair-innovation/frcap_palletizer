import { fileURLToPath, URL } from 'node:url'
import info from './package.json'
import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'

// https://vitejs.dev/config/
export default defineConfig({
  base: './',
  plugins: [
    vue(),
  ],
  resolve: {
    alias: {
      '@': fileURLToPath(new URL('./src', import.meta.url))
    }
  },
  build: {
    terserOptions: {
      compress: {
        drop_console: true,
      },
    },
    chunkSizeWarningLimit: 1500,
    outDir: `./${info.name}-v${info.version}`,
    assetsDir: './assets',
    emptyOutDir: true,
  },
  server: {
    proxy: {
      '/action': {
        target: 'http://localhost:3000',
        changeOrigin: true,
        // rewrite: (path) => path.replace(/^\/action\/plg/, ''),  // 识别api接口后按需截取实际接口，此处使用的api与跨域api一致无须该行代码
      },
    }
  }
})
