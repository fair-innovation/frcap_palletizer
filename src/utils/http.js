import axios from "axios";
import { ElMessage } from "element-plus";
import 'element-plus/dist/index.css';
let palletizingJson;

// 配置axios，创建实例
const http = axios.create({
    baseURL: "",
    // headers: {
    //     'Content-type': 'application/json;charset=UTF-8'
    // },
    timeout: 2000
})

// 请求拦截器
http.interceptors.request.use(
    config => {
        // token && (config.headers.Authorization = token)
        return config 
    },
    error => {
        return Promise.error(error)
    }
)

// 响应拦截器
http.interceptors.response.use(
    response => {
        // 如果返回的状态码为200，说明接口请求成功，可以正常拿到数据
        // 否则的话抛出错误
        console.log(response.status);
        palletizingJson = JSON.parse(localStorage.getItem('messages'));
        // console.log(response.data);
        if (response.status === 200) {
            if (response.data.code === 511) {
                // 未授权调取授权接口
            } else if (response.data.code === 510) {
                // 未登录跳转登录页
            } else {
                ElMessage({
                    message: palletizingJson._success,
                    type: 'success',
                })
                return Promise.resolve(response)
            }
        } else {
            ElMessage({
                message: palletizingJson._fail,
                type: 'error',
            })
            return Promise.reject(response)
        }
    },
    error => {
        palletizingJson = JSON.parse(localStorage.getItem('messages'));
        console.log(error);
        ElMessage({
            message: error.message,
            type: 'error',
        })
        // 可以在这里对异常状态作统一处理
        if (error.response.status) {
            if (error.response.status === 403) {
                ElMessage({
                    message: palletizingJson.error_messages[error.response.data],
                    type: 'error',
                })
            }
            // 处理请求失败的情况
            // 对不同返回码对相应处理
            return Promise.reject(error.response)
        }
    }
)

export default http