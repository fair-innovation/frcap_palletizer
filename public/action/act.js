//var express = require('express');
//var app = express();
//var bodyParser = require('body-parser');
//const events = require('events');
//var multer  = require('multer');
//var fs = require("fs");

var node = "/usr/local/etc/node/sys"
var Socket_Cmd = require(node + '/socket/socket_cmd');
var Socket_Status = require(node + '/socket/socket_status');
var Socket_File = require(node + '/socket/socket_file');
var SqliteDB = require(node + '/sqlite3/sqlite.js').SqliteDB;
var SqliteDB_exec = require(node + '/sqlite3/sqlite.js').SqliteDB_exec;
var SqliteDB_query = require(node + '/sqlite3/sqlite.js').SqliteDB_query;
var Sqlite3_Action = require(node + '/better-sqlite3/better-sqlite3.js');
var File_Handle = require(node + '/tools/file_handle');
var express = require(node + '/tools/express');
var app = express();
var bodyParser = require(node + '/tools/body-parser');
const events = require(node + '/tools/events');
var fs = require(node + '/tools/fs');
var execSync = require(node + '/tools/child_process').execSync;

var sqlite = new Sqlite3_Action();
var file_handle = new File_Handle();

var FAIL = -1;
var SUCCESS = 0; 
var DIR_USER = "/usr/local/etc/web/file/user/";
var DB_POINTS = "/usr/local/etc/web/file/points/web_point.db";
var DIR_PALLETIZING = "/usr/local/etc/web/file/palletizing/";
var DB_PALLETIZING_INIT = "/usr/local/etc/web/file/palletizing/palletizing_init.db";
var DB_PALLETIZING_PROGRAM = "/usr/local/etc/web/file/palletizing/palletizing_program.db";
var palletizing_mode = 0; //0:公司内部测试,1:客户测试
 
// 创建 application/x-www-form-urlencoded 编码解析
//var urlencodedParser = bodyParser.urlencoded({ extended: false })

function wait(second) {
    // execSync 属于同步方法；异步方式请根据需要自行查询 node.js 的 child_process 相关方法；
    var ChildProcess_ExecSync = require('child_process').execSync;
    ChildProcess_ExecSync('sleep ' + second);
};
 
app.use(bodyParser.urlencoded({ extended: false }));
//app.use(multer({ dest: '/tmp/'}).array('image'));


function isJSON(str) {
    if (typeof str == 'string') {
        try {
            var obj=JSON.parse(str);
            if(typeof obj == 'object' && obj ){
                return true;
            }else{
                return false;
            }

        } catch(e) {
            console.log('error：'+str+'!!!'+e);
            return false;
        }
    }
    console.log('It is not a string!')
}

//function generate_per_pattern_file(palletizing_db, p_pattern, pattern_flag, offset_layer_num) {
function generate_per_pattern_file(palletizing_db, pattern_flag) {
    var pattern_lua_name;
    var palletizing_name = "";
    var palletizing_transitionpoint_name;
    var sql = "";
    var data = "";
    var pHome_j1 = "";
    var pHome_j2 = "";
    var pHome_j3 = "";
    var pHome_j4 = "";
    var pHome_j5 = "";
    var pHome_j6 = "";
    //var box_length;
    //var box_width;
    var box_height;
    //var z_offset;
    var grip_height;
    var laminar_separator_enable = 0;
    var laminar_separator_height = 0;
    var layers = 0;
    var sequence = "";
    var array = [];

    sql = "select * from points where name = 'pHome';";
    data = sqlite.queryget(DB_POINTS, sql);
    pHome_j1 = data.j1;
    pHome_j2 = data.j2;
    pHome_j3 = data.j3;
    pHome_j4 = data.j4;
    pHome_j5 = data.j5;
    pHome_j6 = data.j6;

    var sqliteDB = new sqlite.SqliteDB(palletizing_db);

    /* 获取码垛名称 */
    sql = "select * from palletizing_name;";
    data = sqliteDB.queryget(sql);
    palletizing_name = data.name;

    /* 获取隔板配置 */
    sql = "select * from laminar_separator;";
    data = sqliteDB.queryget(sql);
    laminar_separator_enable = data.enable;
    laminar_separator_height = data.height;

    /* 获取工件高度 */
    sql = "select * from box_cfg;";
    data = sqliteDB.queryget(sql);
    box_height = data.height;
    if (laminar_separator_enable == 1) {
        box_height = box_height + laminar_separator_height
    }
    //box_width = data.width;
    //box_length = data.length;
    //z_offset = box_height * offset_layer_num;

    /* 获取高级配置 */
    sql = "select * from advanced_cfg;";
    data = sqliteDB.queryget(sql);
    grip_height = data.height;
    x1 = Number(data.x1);
    y1 = Number(data.y1);
    // 左工位： x-x1, y+y1, z+z1 方向偏移，准备斜插进去
    // 右工位： x-x1, y-y1, z+z1 方向偏移，准备斜插进去
    // 接近点1
    //console.log("y1 = " + y1);
    if (pattern_flag.indexOf("right") !== -1) {
        y1 = (-y1);
    }  
    //console.log("y1 = " + y1); 
    z1 = Number(data.z1);
    x2 = Number(data.x2);
    y2 = Number(data.y2);
    // 左工位： x-x2, y+y2, z+z2 方向偏移，准备斜插进去
    // 右工位： x-x2, y-y2, z+z2 方向偏移，准备斜插进去
    // 接近点2
    //console.log("y2 = " + y2);
    if (pattern_flag.indexOf("right") !== -1) {
        y2 = (-y2);
    }
    //console.log("y2 = " + y2);   
    z2 = Number(data.z2);
    time1 = Number(data.time);
    var lifting_shart_enable = data.lifting_shart_enable;
    var lifting_shart_layer = data.lifting_shart_layer;

    /* 获取码垛模式序列配置 */
    sql = "select * from pattern_sequence_cfg;";
    data = sqliteDB.queryget(sql);
    layers = data.layers;
    sequence = data.sequence;
    array = sequence.split(","); 

    var m;
    var pattern_json;
    var box_ids_array = [];
    var box_grip_point = "";
    var box_IO_1 = 10;
    var box_IO_2 = 11;
    for (m = 0; m < layers; m++) {
        /* 获取码垛模式配置 */
        sql = "select * from pattern_cfg where type = '" + array[m] + "';";
        console.log("sql = " + sql);
        data = sqliteDB.queryget(sql);
        if (pattern_flag.indexOf("left") !== -1) {
            palletizing_transitionpoint_name = palletizing_name + "lefttransitionpoint"; 
            pattern_lua_name = palletizing_name + "left_pattern_" + array[m] + ".lua";
            pattern_json = JSON.parse(data.left_pattern);
        } else {
            palletizing_transitionpoint_name = palletizing_name + "righttransitionpoint";
            pattern_lua_name = palletizing_name + "right_pattern_" + array[m] + ".lua";
            pattern_json = JSON.parse(data.right_pattern);
        }
        box_ids_array = JSON.parse(data.box_ids);
        

        var item = pattern_json[1];
        var p_item = [];
        var p = [];
        var write_content = "";
        var k, j;
        write_content = write_content
        +"layer_index = GetSysVarValue(s_var_4)\n"
        +"box_index = GetSysVarValue(s_var_5)\n"
        +"if box_index == 0 then\n"
        +"     box_index = 1\n"
        +"     SetSysVarValue(s_var_5, box_index)\n"
        +"end\n\n"
  
        // 生成当前层的示教程序
        for (j = 0; j < item.length; j++) {
            // 获取当前层中每个盒子的相关信息，包括盒子抓取点，盒子对应的 IO
            box_grip_point = palletizing_name + "_" + box_ids_array[j] + "_grippoint";
            /* 获取工件 IO  */
            sql = "select IO from box_cfg where box_id = "+ box_ids_array[j] +";";
            var boxIO = sqliteDB.queryget(sql);
            box_IO_1 = JSON.parse(boxIO.IO)[0];
            box_IO_2 = JSON.parse(boxIO.IO)[1];
            console.log("box_IO_1 = " + box_IO_1);
            console.log("box_IO_2 = " + box_IO_2);

            // 获取每一行中的每一个点位
            p_item = item[j];
            for (k = 0; k < 6; k++) {
                p[k] = p_item[k];
            }
            write_content += "-- box id = " + (j+1) + "\n";
            write_content += "box = " + (j+1) + "\n";

            write_content = write_content
            +"if box_index == box then\n"
            +"  start_time = os.time()\n"  
            +"  WaitAuxDI(" + box_IO_1 +",1,0,2)\n"
            +"  WaitAuxDI(" + box_IO_2 +",1,0,2)\n"
            +"  PTP(pHome,100,-1,0)\n"

            // 以 pHome 点，计算接近点1的关节位置
            write_content += "  j1,j2,j3,j4,j5,j6 = GetInverseKin(0," + (p[0] - x1) + "," + (p[1] + y1) + "," + (p[2] + z1) + "," + p[3] + "," + p[4] + "," + p[5] + ",-1)\n";
            // 运动到抓取点 z 上抬 grip_height
            write_content += `  PTP(${box_grip_point},100,-1,2,0,0,-${grip_height},0,0,0)\n`;

            write_content += "  xiqu = 0\n";
            write_content += "  ::box_" + (j+1) + "::do\n";
            write_content += "end\n";

            // 运动到抓取点 z 上抬 grip_height
            write_content += `Lin(${box_grip_point},100,-1,0,2,0,0,-${grip_height},0,0,0)\n`;

            // 运动到抓取点
            write_content += `Lin(${box_grip_point},100,-1,0,0)\n`;
            
            // SetAuxDO 4,1 输出, 吸盘吸取
            write_content += "SetAuxDO(4,1,0,0)\n";
            // 吸料等待时间
            write_content += `WaitMs(${time1})\n`;

            if (palletizing_mode == 1) {
                write_content +=  "if GetToolDI(1,0) == 0 then\n"
            } else {
                write_content +=  "if GetAuxDI(8,0) == 0 then\n"
            }
            write_content = write_content
            +"   SetAuxDO(4,0,0,0)\n"
            +"   if xiqu < 3 then\n"
            +"       xiqu = xiqu + 1\n"
            write_content += "      goto box_" + (j+1) + "\n";
            /*
                "       SetAuxDO(3,1,0,0) --蜂鸣器打开\n"
                "       SetAuxDO(1,0,0,0) --黄色运行灯关闭\n"
                "       SetAuxDO(2,1,0,0) --红色报警灯打开\n"
                "       Pause(2) -- 继续运行弹窗\n"
            */
            write_content = write_content
            +"   else\n"
            +"       SetAuxDO(3,1,0,0)\n"
            +"       SetAuxDO(1,0,0,0)\n"
            +"       SetAuxDO(2,1,0,0)\n"
            +"       Pause(2)\n"
            // 运动到抓取点
            write_content += `      Lin(${box_grip_point},100,-1,0,0)\n`;
            /*
                "       SetAuxDO(3,0,0,0) --蜂鸣器关闭\n"
                "       SetAuxDO(1,1,0,0) --黄色运行灯打开\n"
                "       SetAuxDO(2,0,0,0) --红色报警灯关闭\n"
            */
            write_content = write_content
            +"       SetAuxDO(3,0,0,0)\n"
            +"       SetAuxDO(1,1,0,0)\n"
            +"       SetAuxDO(2,0,0,0)\n"
            +"       xiqu = 0\n"
            write_content += "      goto box_" + (j+1) + "\n";
            write_content = write_content
            +"  end\n"
            +"end\n"

            // 运动到抓取点 z 上抬 grip_height
            write_content += `Lin(${box_grip_point},100,-1,0,2,0,0,-${grip_height},0,0,0)\n`;
            
            // 增加 offset
            if (lifting_shart_enable == 1) {
                write_content = write_content
                    +`if layer_index < ${lifting_shart_layer} then\n`
                    +`      PointsOffsetEnable(0,0,0,${box_height}*(layer_index-1),0,0,0)\n`
                    +"else\n"
                    //+`        zero_offset_point = "${palletizing_name}" .. "lifting_shartzero" .. "(layer_index-${lifting_shart_layer}+1)\n"`
                    //+`        EXT_AXIS_PTP(0, zero_offset_point, 30)\n`
                    // TODO: 升降柱偏移为 zero300（工件高度） + 隔板高度
                    +"      EXT_AXIS_PTP(0,zero300,30)\n"
                    +`      PointsOffsetEnable(0,0,0,${box_height}*(${lifting_shart_layer}-2),0,0,0)\n`
                    +"end\n"
            } else {
                write_content += `PointsOffsetEnable(0,0,0,${box_height}*(layer_index-1),0,0,0)\n`
            }

            // 运动到过度点
            write_content += `PTP(${palletizing_transitionpoint_name},100,-1,0)\n`

            // 运动接近点1，准备斜插进去
            write_content += "MoveJ(j1,j2,j3,j4,j5,j6," + (p[0] - x1) + "," + (p[1] + y1) + "," + (p[2] + z1) + "," + p[3] + "," + p[4] + "," + p[5] + ",1,0,100,100,100,0,0,0,0,-1,0,0,0,0,0,0,0)\n";
            write_content += "::box_" + (j+1) + "_2::do\n";
            write_content += "end\n";

            // 运动到接近点2: 准备斜插进去
            write_content += "j1,j2,j3,j4,j5,j6 = GetInverseKin(0," + (p[0] - x2) + "," + (p[1] + y2) + "," + (p[2] + z2) + "," + p[3] + "," + p[4] + "," + p[5] + ",-1)\n";
            write_content += "MoveL(j1,j2,j3,j4,j5,j6," + (p[0] - x2) + "," + (p[1] + y2) + "," + (p[2] + z2) + "," + p[3] + "," + p[4] + "," + p[5] + ",1,0,100,100,100,-1,0,0,0,0,0,0,0,0,0,0,0,0)\n";

            // 运动到放置点 z
            write_content += "j1,j2,j3,j4,j5,j6 = GetInverseKin(0," + p[0] + "," + p[1] + "," + p[2] + "," + p[3] + "," + p[4] + "," + p[5] + ",-1)\n";
            write_content += "MoveL(j1,j2,j3,j4,j5,j6," + p[0] + "," + p[1] + "," + p[2] + "," + p[3] + "," + p[4] + "," + p[5] + ",1,0,100,100,100,-1,0,0,0,0,0,0,0,0,0,0,0,0)\n";

            // SetAuxDO 4,0 输出, 吸盘释放
            write_content = write_content
            +"SetAuxDO(4,0,0,0)\n"
            +"WaitMs(500)\n"

            //"--**放料负压判断**--\n"
            //" SetAuxDO(3,1,0,0) --蜂鸣器打开\n"
            //" SetAuxDO(1,0,0,0) --黄色运行灯关闭\n"
            //" SetAuxDO(2,1,0,0) --红色报警灯打开\n"
            //" Pause(2) -- 放料负压检测弹窗\n"
            if (palletizing_mode == 1) {
                write_content +=  "if GetToolDI(1,0) == 1 then\n"
            } else {
                write_content +=  "if GetAuxDI(8,0) == 1 then\n"
            }
            write_content = write_content
            +"    SetAuxDO(3,1,0,0)\n"
            +"    SetAuxDO(1,0,0,0)\n"
            +"    SetAuxDO(2,1,0,0)\n"
            +"    Pause(2)\n"
            write_content += "    MoveL(j1,j2,j3,j4,j5,j6," + p[0] + "," + p[1] + "," + (p[2]+5) + "," + p[3] + "," + p[4] + "," + p[5] + ",1,0,100,100,100,-1,0,0,0,0,0,0,0,0,0,0,0,0)\n";
            //" SetAuxDO(3,0,0,0) --蜂鸣器关闭\n"
            //" SetAuxDO(1,1,0,0) --黄色运行灯打开\n"
            //" SetAuxDO(2,0,0,0) --红色报警灯关闭\n"
            write_content = write_content
            +"    SetAuxDO(3,0,0,0)\n"
            +"    SetAuxDO(1,1,0,0)\n"
            +"    SetAuxDO(2,0,0,0)\n"
            // 运动到抓取点: 放置点 z- = 10
            write_content += "    j1,j2,j3,j4,j5,j6 = GetInverseKin(0," + p[0] + "," + p[1] + "," + (p[2] - 10) + "," + p[3] + "," + p[4] + "," + p[5] + ",-1)\n";
            write_content += "    MoveL(j1,j2,j3,j4,j5,j6," + p[0] + "," + p[1] + "," + (p[2]-10) + "," + p[3] + "," + p[4] + "," + p[5] + ",1,0,100,100,100,-1,0,0,0,0,0,0,0,0,0,0,0,0)\n";
            write_content += "    SetAuxDO(4,1,0,0)\n";
            write_content += `    WaitMs(${time1})\n`;
            // 运动到放置点: z+ = box_height
            write_content += "    j1,j2,j3,j4,j5,j6 = GetInverseKin(0," + p[0] + "," + p[1] + "," + (p[2] + box_height) + "," + p[3] + "," + p[4] + "," + p[5] + ",-1)\n";
            write_content += "    MoveL(j1,j2,j3,j4,j5,j6," + p[0] + "," + p[1] + "," + (p[2] + box_height) + "," + p[3] + "," + p[4] + "," + p[5] + ",1,0,100,100,100,-1,0,0,0,0,0,0,0,0,0,0,0,0)\n";
            write_content += "    goto box_" + (j+1) + "_2\n";
            write_content += "end\n"

            write_content = write_content
            +"product_count = GetSysVarValue(s_var_1) + 1\n"
            +"SetSysVarValue(s_var_1, product_count)\n"
            +"box_index = GetSysVarValue(s_var_5) + 1\n"
            +"SetSysVarValue(s_var_5, box_index)\n"

            if (pattern_flag.indexOf("left") !== -1) {
                write_content = write_content
                +"left_product_count = GetSysVarValue(s_var_6) + 1\n"
                +"SetSysVarValue(s_var_6, left_product_count)\n"
            } else {
                write_content = write_content
                +"right_product_count = GetSysVarValue(s_var_7) + 1\n"
                +"SetSysVarValue(s_var_7, right_product_count)\n"
            }

            // 运动到放置点: z+ = box_height
            write_content += "j1,j2,j3,j4,j5,j6 = GetInverseKin(0," + p[0] + "," + p[1] + "," + (p[2] + box_height) + "," + p[3] + "," + p[4] + "," + p[5] + ",-1)\n";
            write_content += "MoveL(j1,j2,j3,j4,j5,j6," + p[0] + "," + p[1] + "," + (p[2] + box_height) + "," + p[3] + "," + p[4] + "," + p[5] + ",1,0,100,100,100,-1,0,0,0,0,0,0,0,0,0,0,0,0)\n";

            // 运动到过度点
            write_content += `PTP(${palletizing_transitionpoint_name},100,-1,0)\n`


            write_content += "PointsOffsetDisable()\n"
            // 回原点
            write_content = write_content
            +"PTP(pHome,100,-1,0)\n"
            if (lifting_shart_enable == 1) {
                write_content += "EXT_AXIS_PTP(0,zero,30)\n"
            }
            write_content = write_content
            +"total_time = GetSysVarValue(s_var_2)\n"
            +"end_time = os.time()\n"
            +"total_time = total_time + end_time - start_time\n"
            +"SetSysVarValue(s_var_2, total_time)\n"
            +"single_time = total_time/product_count\n"
            +"SetSysVarValue(s_var_3, single_time)\n"
            +"end\n\n"
        }

        var filename = `${DIR_USER}${pattern_lua_name}`;
        console.log("filename = " + filename);
        //console.log("write_content:\n" + write_content);
        file_handle.write(filename, write_content);
        file_handle.chmod(filename, 0o777);
    }

    sqliteDB.close();

    return SUCCESS;
}

//function generate_per_depattern_file(palletizing_db, p_pattern, pattern_flag, offset_layer_num) {
function generate_per_depattern_file(palletizing_db, pattern_flag) {
    var pattern_lua_name;
    var palletizing_name = "";
    var palletizing_transitionpoint_name;
    var sql = "";
    var data = "";
    var pHome_j1 = "";
    var pHome_j2 = "";
    var pHome_j3 = "";
    var pHome_j4 = "";
    var pHome_j5 = "";
    var pHome_j6 = "";
    //var box_length;
    //var box_width;
    var box_height;
    //var z_offset;
    var grip_height;
    var laminar_separator_enable = 0;
    var laminar_separator_height = 0;
    var layers = 0;
    var sequence = "";
    var array = [];

    sql = "select * from points where name = 'pHome';";
    data = sqlite.queryget(DB_POINTS, sql);
    pHome_j1 = data.j1;
    pHome_j2 = data.j2;
    pHome_j3 = data.j3;
    pHome_j4 = data.j4;
    pHome_j5 = data.j5;
    pHome_j6 = data.j6;

    var sqliteDB = new sqlite.SqliteDB(palletizing_db);

    /* 获取码垛名称 */
    sql = "select * from palletizing_name;";
    data = sqliteDB.queryget(sql);
    palletizing_name = data.name;

    /* 获取隔板配置 */
    sql = "select * from laminar_separator;";
    data = sqliteDB.queryget(sql);
    laminar_separator_enable = data.enable;
    laminar_separator_height = data.height;

    /* 获取工件高度 */
    sql = "select * from box_cfg;";
    data = sqliteDB.queryget(sql);
    box_height = data.height;
    if (laminar_separator_enable == 1) {
        box_height = box_height + laminar_separator_height
    }
    //box_width = data.width;
    //box_length = data.length;
    //z_offset = box_height * offset_layer_num;

    /* 获取高级配置 */
    sql = "select * from advanced_cfg;";
    data = sqliteDB.queryget(sql);
    grip_height = data.height;
    x1 = Number(data.x1);
    y1 = Number(data.y1);
    // 左工位： x-x1, y+y1, z+z1 方向偏移，准备斜插进去
    // 右工位： x-x1, y-y1, z+z1 方向偏移，准备斜插进去
    // 接近点1
    //console.log("y1 = " + y1);
    if (pattern_flag.indexOf("right") !== -1) {
        y1 = (-y1);
    }  
    //console.log("y1 = " + y1); 
    z1 = Number(data.z1);
    x2 = Number(data.x2);
    y2 = Number(data.y2);
    // 左工位： x-x2, y+y2, z+z2 方向偏移，准备斜插进去
    // 右工位： x-x2, y-y2, z+z2 方向偏移，准备斜插进去
    // 接近点2
    //console.log("y2 = " + y2);
    if (pattern_flag.indexOf("right") !== -1) {
        y2 = (-y2);
    }
    //console.log("y2 = " + y2);   
    z2 = Number(data.z2);
    time1 = Number(data.time);
    var lifting_shart_enable = data.lifting_shart_enable;
    var lifting_shart_layer = data.lifting_shart_layer;

    /* 获取码垛模式序列配置 */
    sql = "select * from pattern_sequence_cfg;";
    data = sqliteDB.queryget(sql);
    layers = data.layers;
    sequence = data.sequence;
    array = sequence.split(",");

    var m;
    var pattern_json;
    var box_ids_array = [];
    var box_grip_point = "";
    var box_IO_1 = 10;
    var box_IO_2 = 11;
    for (m = 0; m < layers; m++) {
        /* 获取码垛模式配置 */
        sql = "select * from pattern_cfg where type = '" + array[m] + "';";
        data = sqliteDB.queryget(sql); 
        if (pattern_flag.indexOf("left") !== -1) {
            palletizing_transitionpoint_name = palletizing_name + "lefttransitionpoint"; 
            pattern_lua_name = palletizing_name + "left_depattern_" + array[m] + ".lua";
            pattern_json = JSON.parse(data.left_pattern);
        } else {
            palletizing_transitionpoint_name = palletizing_name + "righttransitionpoint";
            pattern_lua_name = palletizing_name + "right_depattern_" + array[m] + ".lua";
            pattern_json = JSON.parse(data.right_pattern);
        }
        box_ids_array = JSON.parse(data.box_ids);

        var item = pattern_json[1];
        var p_item = [];
        var p = [];
        var write_content = "";
        var k, j;
        write_content = write_content
        +"layer_index = GetSysVarValue(s_var_4)\n"
        +"box_index = GetSysVarValue(s_var_5)\n"
        +"if box_index == 0 then\n"
        write_content += "    box_index = " + item.length + "\n"
        write_content = write_content
        +"    SetSysVarValue(s_var_5, box_index)\n"
        +"end\n\n"

        // 生成当前层的示教程序
        for (j = (item.length-1); j >= 0; j--) {
            // 获取当前层中每个盒子的相关信息，包括盒子抓取点，盒子对应的 IO
            box_grip_point = palletizing_name + "_" + box_ids_array[j] + "_grippoint";
            /* 获取工件 IO  */
            sql = "select IO from box_cfg where box_id = "+ box_ids_array[j] +";";
            var boxIO = sqliteDB.queryget(sql);
            box_IO_1 = JSON.parse(boxIO.IO)[0];
            box_IO_2 = JSON.parse(boxIO.IO)[1];
            console.log("box_IO_1 = " + box_IO_1);
            console.log("box_IO_2 = " + box_IO_2);
            // 获取每一行中的每一个点位
            p_item = item[j];
            for (k = 0; k < 6; k++) {
                p[k] = p_item[k];
            }
            write_content += "-- box id = " + (j+1) + "\n";
            write_content += "box = " + (j+1) + "\n";

            write_content = write_content
            +"if box_index == box then\n"
            +"  start_time = os.time()\n"
            +"  PTP(pHome,100,-1,0)\n"

            write_content += "  j1,j2,j3,j4,j5,j6 = GetInverseKin(0," + (p[0]) + "," + (p[1]) + "," + (p[2] + box_height) + "," + p[3] + "," + p[4] + "," + p[5] + ",-1)\n";
           
            // 增加 offset
            if (lifting_shart_enable == 1) {
                write_content = write_content
                    +`  if layer_index < ${lifting_shart_layer} then\n`
                    +`        PointsOffsetEnable(0,0,0,${box_height}*(layer_index-1),0,0,0)\n`
                    +"  else\n"
                    //+`        zero_offset_point = "${palletizing_name}" .. "lifting_shartzero" .. "(layer_index-${lifting_shart_layer}+1)\n"`
                    //+`        EXT_AXIS_PTP(0, zero_offset_point, 30)\n`
                    // TODO: 升降柱偏移为 zero300（工件高度） + 隔板高度
                    +"        EXT_AXIS_PTP(0,zero300,30)\n"
                    +`        PointsOffsetEnable(0,0,0,${box_height}*(${lifting_shart_layer}-2),0,0,0)\n`
                    +"  end\n"
            } else {
                write_content += `  PointsOffsetEnable(0,0,0,${box_height}*(layer_index-1),0,0,0)\n`
            }
            // 运动到过度点
            write_content += `  PTP(${palletizing_transitionpoint_name},100,-1,0)\n`
            write_content += "  xiqu = 0\n";
            write_content += "  ::box_" + (j+1) + "::do\n";
            write_content += "  end\n";

            // 运动到抓取点: z+ = box_height
            write_content += "  MoveJ(j1,j2,j3,j4,j5,j6," + p[0] + "," + p[1] + "," + (p[2] + box_height) + "," + p[3] + "," + p[4] + "," + p[5] + ",1,0,100,100,100,0,0,0,0,-1,0,0,0,0,0,0,0)\n";

            // 运动到抓取点: z -= 10
            write_content += "  j1,j2,j3,j4,j5,j6 = GetInverseKin(0," + p[0] + "," + p[1] + "," + (p[2] - 10) + "," + p[3] + "," + p[4] + "," + p[5] + ",-1)\n";
            write_content += "  MoveL(j1,j2,j3,j4,j5,j6," + p[0] + "," + p[1] + "," + (p[2]-10) + "," + p[3] + "," + p[4] + "," + p[5] + ",1,0,100,100,100,-1,0,0,0,0,0,0,0,0,0,0,0,0)\n";
            // SetAuxDO 4,1 输出, 吸盘吸取
            write_content += "  SetAuxDO(4,1,0,0)\n";
            // 吸料等待时间
            write_content += `  WaitMs(${time1})\n`;

            //"--**吸取负压判断**--"
            if (palletizing_mode == 1) {
                write_content +=  "if GetToolDI(1,0) == 0 then\n"
            } else {
                write_content +=  "if GetAuxDI(8,0) == 0 then\n"
            }
            write_content = write_content
            +"   SetAuxDO(4,0,0,0)\n"
            +"   if xiqu < 3 then\n"
            +"       xiqu = xiqu + 1\n"
            write_content += "      goto box_" + (j+1) + "\n";
            write_content = write_content
            +"   else\n"
            +"       SetAuxDO(3,1,0,0)\n"
            +"       SetAuxDO(1,0,0,0)\n"
            +"       SetAuxDO(2,1,0,0)\n"
            +"       Pause(2)\n"
            write_content += "         MoveL(j1,j2,j3,j4,j5,j6," + p[0] + "," + p[1] + "," + (p[2]-10) + "," + p[3] + "," + p[4] + "," + p[5] + ",1,0,100,100,100,-1,0,0,0,0,0,0,0,0,0,0,0,0)\n";
            write_content = write_content
            +"       SetAuxDO(3,0,0,0)\n"
            +"       SetAuxDO(1,1,0,0)\n"
            +"       SetAuxDO(2,0,0,0)\n"
            +"       xiqu = 0\n"
            write_content += "      goto box_" + (j+1) + "\n";
            write_content = write_content
            +"  end\n"
            +"end\n"

            // 运动到接近点2，准备斜插出来
            write_content += "j1,j2,j3,j4,j5,j6 = GetInverseKin(0," + (p[0] - x2) + "," + (p[1] + y2) + "," + (p[2] + z2) + "," + p[3] + "," + p[4] + "," + p[5] + ",-1)\n";
            write_content += "MoveL(j1,j2,j3,j4,j5,j6," + (p[0] - x2) + "," + (p[1] + y2) + "," + (p[2] + z2) + "," + p[3] + "," + p[4] + "," + p[5] + ",1,0,100,100,100,-1,0,0,0,0,0,0,0,0,0,0,0,0)\n";

            // 运动到接近点1，准备斜插出来
            write_content += "j1,j2,j3,j4,j5,j6 = GetInverseKin(0," + (p[0] - x1) + "," + (p[1] + y1) + "," + (p[2] + z1) + "," + p[3] + "," + p[4] + "," + p[5] + ",-1)\n";
            write_content += "MoveL(j1,j2,j3,j4,j5,j6," + (p[0] - x1) + "," + (p[1] + y1) + "," + (p[2] + z1) + "," + p[3] + "," + p[4] + "," + p[5] + ",1,0,100,100,100,-1,0,0,0,0,0,0,0,0,0,0,0,0)\n";

            // 运动到过度点
            write_content += `PTP(${palletizing_transitionpoint_name},100,-1,0)\n`

            write_content += "PointsOffsetDisable()\n"

            // 运动到抓取点 z 上抬 grip_height
            write_content += `PTP(${box_grip_point},100,-1,2,0,0,-${grip_height},0,0,0)\n`;
            if (lifting_shart_enable == 1) {
                write_content += "EXT_AXIS_PTP(0,zero,30)\n"
            }
            write_content += "::box_" + (j+1) + "_2::do\n";
            write_content += "end\n";

            // 运动到放置点 -z = 10
            write_content += `Lin(${box_grip_point},100,-1,0,2,0,0,-10,0,0,0)\n`;
            // SetAuxDO 4,0 输出, 吸盘释放
            write_content = write_content
            +"SetAuxDO(4,0,0,0)\n"
            +"WaitMs(500)\n"
            //"--**放料负压判断**--\n"
            if (palletizing_mode == 1) {
                write_content += "if GetToolDI(1,0) == 1 then\n"
            } else {
                write_content += "if GetAuxDI(8,0) == 1 then\n"
            }
            write_content = write_content
            +"    SetAuxDO(3,1,0,0)\n"
            +"    SetAuxDO(1,0,0,0)\n"
            +"    SetAuxDO(2,1,0,0)\n"
            +"    Pause(2)\n"
            write_content += `    Lin(${box_grip_point},100,-1,0,2,0,0,-10,0,0,0)\n`;
            write_content = write_content
            +"    SetAuxDO(3,0,0,0)\n"
            +"    SetAuxDO(1,1,0,0)\n"
            +"    SetAuxDO(2,0,0,0)\n"
            // 运动到抓取点
            write_content += `    Lin(${box_grip_point},100,-1,0,0)\n`;
            write_content += "    SetAuxDO(4,1,0,0)\n";
            write_content += `    WaitMs(${time1})\n`;
            // 直线运动到抓取点 z 上抬 grip_height
            write_content += `    Lin(${box_grip_point},100,-1,0,2,0,0,-${grip_height},0,0,0)\n`;
            write_content += "    goto box_" + (j+1) + "_2\n";
            write_content += "end\n"
            write_content = write_content
            +"product_count = GetSysVarValue(s_var_1) + 1\n"
            +"SetSysVarValue(s_var_1, product_count)\n"
            +"box_index = GetSysVarValue(s_var_5) - 1\n"
            +"SetSysVarValue(s_var_5, box_index)\n"

            if (pattern_flag.indexOf("left") !== -1) {
                write_content = write_content
                +"left_product_count = GetSysVarValue(s_var_6) + 1\n"
                +"SetSysVarValue(s_var_6, left_product_count)\n"
            } else {
                write_content = write_content
                +"right_product_count = GetSysVarValue(s_var_7) + 1\n"
                +"SetSysVarValue(s_var_7, right_product_count)\n"
            }
            // 直线运动到抓取点 z 上抬 grip_height
            write_content += `Lin(${box_grip_point},100,-1,0,2,0,0,-${grip_height},0,0,0)\n`;
            // 回原点
            write_content = write_content
            +"PTP(pHome,100,-1,0)\n"
            +"total_time = GetSysVarValue(s_var_2)\n"
            +"end_time = os.time()\n"
            +"total_time = total_time + end_time - start_time\n"
            +"SetSysVarValue(s_var_2, total_time)\n"
            +"single_time = total_time/product_count\n"
            +"SetSysVarValue(s_var_3, single_time)\n"
            +"end\n\n"
        }

        var filename = `${DIR_USER}${pattern_lua_name}`;
        console.log("filename = " + filename);
        //console.log("write_content:\n" + write_content);
        file_handle.write(filename, write_content);
        file_handle.chmod(filename, 0o777);
    }

    sqliteDB.close();

    return SUCCESS;
}

/* 生成隔板码垛程序 */
function generate_per_laminar_separator_file(palletizing_db, prefix) {
    var transitionpoint1_name = "";
    var transitionpoint2_name = "";
    var transitionpoint3_name = "";
    var grip_name = "";
    var place_name = "";
    var lua_name = "";
    var write_content = "";
    var sql = "";
    var data = "";
    var time1 = 0;
    var box_height;
    var lifting_shart_enable = 0;
    var lifting_shart_layer = 0;
    var laminar_separator_enable = 0;
    var laminar_separator_height = 0;

    var sqliteDB = new sqlite.SqliteDB(palletizing_db);

    sql = "select * from advanced_cfg;";
    data = sqliteDB.queryget(sql);
    time1 = Number(data.time);
    lifting_shart_enable = data.lifting_shart_enable;
    lifting_shart_layer = data.lifting_shart_layer;

    /* 获取隔板配置 */
    sql = "select * from laminar_separator;";
    data = sqliteDB.queryget(sql);
    laminar_separator_enable = data.enable;
    laminar_separator_height = data.height;

    /* 获取工件高度 */
    sql = "select * from box_cfg;";
    data = sqliteDB.queryget(sql);
    box_height = data.height;
    if (laminar_separator_enable == 1) {
        box_height = box_height + laminar_separator_height
    }

    transitionpoint_name1 = prefix + "_transitionpoint1";
    transitionpoint_name2 = prefix + "_transitionpoint2";
    transitionpoint_name3 = prefix + "_transitionpoint3";
    grip_name = prefix + "_grippoint";
    place_name = prefix + "_placepoint";

    write_content = write_content
    +"layer_index = GetSysVarValue(s_var_4)\n"

    write_content = write_content +"PTP(pHome,100,-1,0)\n"

    write_content += `PTP(${transitionpoint_name1},100,-1,0)\n`

    // 运动到抓取点 z 上抬 200 
    write_content += `PTP(${grip_name},100,-1,2,0,0,-200,0,0,0)\n`;

    write_content += "xiqu = 0\n";
    write_content += "::laminar_separator::do\n";
    write_content += "end\n";

    // 运动到抓取点 z 上抬 200 
    write_content += `Lin(${grip_name},100,-1,0,2,0,0,-200,0,0,0)\n`;
    
    // 运动到抓取点
    write_content += `Lin(${grip_name},100,-1,0,2,0,0,${laminar_separator_height}*(layer_index-1),0,0,0)\n`;
    
    // SetAuxDO 4,1 输出, 吸盘吸取
    write_content += "SetAuxDO(4,1,0,0)\n";
    // 吸料等待时间
    write_content += `WaitMs(${time1})\n`;

    if (palletizing_mode == 1) {
        write_content +=  "if GetToolDI(1,0) == 0 then\n"
    } else {
        write_content +=  "if GetAuxDI(8,0) == 0 then\n"
    }
    write_content = write_content
    +"   SetAuxDO(4,0,0,0)\n"
    +"   if xiqu < 3 then\n"
    +"       xiqu = xiqu + 1\n"
    write_content += "       goto laminar_separator\n";
    /*
        "       SetAuxDO(3,1,0,0) --蜂鸣器打开\n"
        "       SetAuxDO(1,0,0,0) --黄色运行灯关闭\n"
        "       SetAuxDO(2,1,0,0) --红色报警灯打开\n"
        "       Pause(2) -- 继续运行弹窗\n"
    */
    write_content = write_content
    +"   else\n"
    +"       SetAuxDO(3,1,0,0)\n"
    +"       SetAuxDO(1,0,0,0)\n"
    +"       SetAuxDO(2,1,0,0)\n"
    +"       Pause(2)\n"
    // 运动到抓取点
    write_content += `       Lin(${grip_name},100,-1,0,2,0,0,-200,0,0,0)\n`;
    /*
        "       SetAuxDO(3,0,0,0) --蜂鸣器关闭\n"
        "       SetAuxDO(1,1,0,0) --黄色运行灯打开\n"
        "       SetAuxDO(2,0,0,0) --红色报警灯关闭\n"
    */
    write_content = write_content
    +"       SetAuxDO(3,0,0,0)\n"
    +"       SetAuxDO(1,1,0,0)\n"
    +"       SetAuxDO(2,0,0,0)\n"
    +"       xiqu = 0\n"
    write_content += "       goto laminar_separator\n";
    write_content = write_content
    +"  end\n"
    +"end\n"

    // 运动到抓取点 z 上抬 200 
    write_content += `Lin(${grip_name},100,-1,0,2,0,0,-200,0,0,0)\n`;

    write_content += `PTP(${transitionpoint_name1},100,-1,0)\n`

    write_content += `PTP(${transitionpoint_name2},100,-1,0)\n`

    // 增加 offset
    if (lifting_shart_enable == 1) {
        write_content = write_content
            +`if layer_index < ${lifting_shart_layer} then\n`
            +`      PointsOffsetEnable(0,0,0,${box_height}*(layer_index-1),0,0,0)\n`
            +"else\n"
            //+`        zero_offset_point = "${palletizing_name}" .. "lifting_shartzero" .. "(layer_index-${lifting_shart_layer}+1)\n"`
            //+`        EXT_AXIS_PTP(0, zero_offset_point, 30)\n`
            // TODO: 升降柱偏移为 zero300（工件高度） + 隔板高度
            +"      EXT_AXIS_PTP(0,zero300,30)\n"
            +`      PointsOffsetEnable(0,0,0,${box_height}*(${lifting_shart_layer}-2),0,0,0)\n`
            +"end\n"
    } else {
        write_content += `PointsOffsetEnable(0,0,0,${box_height}*(layer_index-1),0,0,0)\n`
    }

    write_content += `PTP(${transitionpoint_name3},100,-1,0)\n`

    write_content += `PTP(${place_name},100,-1,2,0,0,-200,0,0,0)\n`;

    write_content += "::laminar_separator_2::do\n";
    write_content += "end\n";

    write_content += `Lin(${place_name},100,-1,0,0)\n`;

    // SetAuxDO 4,0 输出, 吸盘释放
    write_content = write_content
    +"SetAuxDO(4,0,0,0)\n"
    +"WaitMs(500)\n"

    //"--**放料负压判断**--\n"
    if (palletizing_mode == 1) {
        write_content += "if GetToolDI(1,0) == 1 then\n"
    } else {
        write_content += "if GetAuxDI(8,0) == 1 then\n"
    }
    write_content = write_content
    +"   SetAuxDO(3,1,0,0)\n"
    +"   SetAuxDO(1,0,0,0)\n"
    +"   SetAuxDO(2,1,0,0)\n"
    +"   Pause(2)\n"
    write_content = write_content
    +"   SetAuxDO(3,0,0,0)\n"
    +"   SetAuxDO(1,1,0,0)\n"
    +"   SetAuxDO(2,0,0,0)\n"
    // 运动到放置点 z + 10
    write_content += `   Lin(${place_name},100,-1,0,2,0,0,10,0,0,0)\n`;
    write_content += "   SetAuxDO(4,1,0,0)\n";
    write_content += `   WaitMs(${time1})\n`;
    write_content += `   Lin(${place_name},100,-1,0,2,0,0,-200,0,0,0)\n`;
    write_content += "   goto laminar_separator_2\n";
    write_content += "end\n"

    write_content += `Lin(${place_name},100,-1,0,2,0,0,-200,0,0,0)\n`;
    write_content += `PTP(${transitionpoint_name3},100,-1,0)\n`

    //结束偏移
    write_content += "PointsOffsetDisable()\n"
    // 回原点
    write_content = write_content
    +"PTP(pHome,100,-1,0)\n"
    if (lifting_shart_enable == 1) {
        write_content += "EXT_AXIS_PTP(0,zero,30)\n"
    }

    var filename = `${DIR_USER}${prefix}.lua`;
    console.log("filename = " + filename);
    //console.log("write_content:\n" + write_content);
    file_handle.write(filename, write_content);
    file_handle.chmod(filename, 0o777);

    sqliteDB.close(); 

    return SUCCESS;
}

/* 生成隔板拆垛程序 */
function generate_per_delaminar_separator_file(palletizing_db, prefix) {
    var transitionpoint1_name = "";
    var transitionpoint2_name = "";
    var transitionpoint3_name = "";
    var grip_name = "";
    var place_name = "";
    var lua_name = "";
    var write_content = "";
    var sql = "";
    var data = "";
    var time1 = 0;
    var box_height;
    var lifting_shart_enable = 0;
    var lifting_shart_layer = 0;
    var laminar_separator_enable = 0;
    var laminar_separator_height = 0;

    var sqliteDB = new sqlite.SqliteDB(palletizing_db);

    sql = "select * from advanced_cfg;";
    data = sqliteDB.queryget(sql);
    time1 = Number(data.time);
    lifting_shart_enable = data.lifting_shart_enable;
    lifting_shart_layer = data.lifting_shart_layer;

    /* 获取隔板配置 */
    sql = "select * from laminar_separator;";
    data = sqliteDB.queryget(sql);
    laminar_separator_enable = data.enable;
    laminar_separator_height = data.height;

    /* 获取工件高度 */
    sql = "select * from box_cfg;";
    data = sqliteDB.queryget(sql);
    box_height = data.height;
    if (laminar_separator_enable == 1) {
        box_height = box_height + laminar_separator_height
    }

    transitionpoint_name1 = prefix + "_transitionpoint1";
    transitionpoint_name2 = prefix + "_transitionpoint2";
    transitionpoint_name3 = prefix + "_transitionpoint3";
    grip_name = prefix + "_grippoint";
    place_name = prefix + "_placepoint";

    write_content = write_content
    +"layer_index = GetSysVarValue(s_var_4)\n"

    write_content = write_content +"PTP(pHome,100,-1,0)\n"
    // 增加 offset
    if (lifting_shart_enable == 1) {
        write_content = write_content
            +`if layer_index < ${lifting_shart_layer} then\n`
            +`      PointsOffsetEnable(0,0,0,${box_height}*(layer_index-1),0,0,0)\n`
            +"else\n"
            //+`        zero_offset_point = "${palletizing_name}" .. "lifting_shartzero" .. "(layer_index-${lifting_shart_layer}+1)\n"`
            //+`        EXT_AXIS_PTP(0, zero_offset_point, 30)\n`
            // TODO: 升降柱偏移为 zero300（工件高度） + 隔板高度
            +"      EXT_AXIS_PTP(0,zero300,30)\n"
            +`      PointsOffsetEnable(0,0,0,${box_height}*(${lifting_shart_layer}-2),0,0,0)\n`
            +"end\n"
    } else {
        write_content += `PointsOffsetEnable(0,0,0,${box_height}*(layer_index-1),0,0,0)\n`
    }

    write_content += `PTP(${transitionpoint_name3},100,-1,0)\n`

    write_content += "xiqu = 0\n";
    write_content += "::laminar_separator::do\n";
    write_content += "end\n";

    write_content += `PTP(${place_name},100,-1,2,0,0,-200,0,0,0)\n`;
    write_content += `Lin(${place_name},100,-1,0,0)\n`;
    // SetAuxDO 4,1 输出, 吸盘吸取
    write_content += "SetAuxDO(4,1,0,0)\n";
    // 吸料等待时间
    write_content += `WaitMs(${time1})\n`;

    //"--**吸取负压判断**--"
    if (palletizing_mode == 1) {
        write_content +=  "if GetToolDI(1,0) == 0 then\n"
    } else {
        write_content +=  "if GetAuxDI(8,0) == 0 then\n"
    }
    write_content = write_content
    +"   SetAuxDO(4,0,0,0)\n"
    +"   if xiqu < 3 then\n"
    +"       xiqu = xiqu + 1\n"
    write_content += "      goto laminar_separator\n";
    write_content = write_content
    +"   else\n"
    +"       SetAuxDO(3,1,0,0)\n"
    +"       SetAuxDO(1,0,0,0)\n"
    +"       SetAuxDO(2,1,0,0)\n"
    +"       Pause(2)\n"
    write_content += `       Lin(${place_name},100,-1,0,2,0,0,-200,0,0,0)\n`;
    write_content = write_content
    +"       SetAuxDO(3,0,0,0)\n"
    +"       SetAuxDO(1,1,0,0)\n"
    +"       SetAuxDO(2,0,0,0)\n"
    +"       xiqu = 0\n"
    write_content += "      goto laminar_separator\n";
    write_content = write_content
    +"  end\n"
    +"end\n"

    write_content += `Lin(${place_name},100,-1,0,2,0,0,-200,0,0,0)\n`;

    write_content += `PTP(${transitionpoint_name3},100,-1,0)\n`

    write_content += `PTP(${transitionpoint_name2},100,-1,0)\n`

    //结束偏移
    write_content += "PointsOffsetDisable()\n"

    if (lifting_shart_enable == 1) {
        write_content += "EXT_AXIS_PTP(0,zero,30)\n"
    }

    write_content += `PTP(${transitionpoint_name1},100,-1,0)\n`

    write_content += `PTP(${grip_name},100,-1,2,0,0,-200,0,0,0)\n`;

    write_content += "::laminar_separator_2::do\n";
    write_content += "end\n";

    write_content += `PTP(${grip_name},100,-1,2,0,0,((-1)*${laminar_separator_height}*(layer_index-1)-10),0,0,0)\n`;
    //write_content += `Lin(${grip_name},100,-1,0,0)\n`;

    // SetAuxDO 4,0 输出, 吸盘释放
    write_content = write_content
    +"SetAuxDO(4,0,0,0)\n"
    +"WaitMs(500)\n"

    //"--**放料负压判断**--\n"
    if (palletizing_mode == 1) {
        write_content += "if GetToolDI(1,0) == 1 then\n"
    } else {
        write_content += "if GetAuxDI(8,0) == 1 then\n"
    }
    write_content = write_content
    +"   SetAuxDO(3,1,0,0)\n"
    +"   SetAuxDO(1,0,0,0)\n"
    +"   SetAuxDO(2,1,0,0)\n"
    +"   Pause(2)\n"
    write_content = write_content
    +"   SetAuxDO(3,0,0,0)\n"
    +"   SetAuxDO(1,1,0,0)\n"
    +"   SetAuxDO(2,0,0,0)\n"
    // 运动到抓取点
    write_content += `   Lin(${grip_name},100,-1,0,2,0,0,((-1)*${laminar_separator_height}*(layer_index-1)-10),0,0,0)\n`;
    write_content += "   SetAuxDO(4,1,0,0)\n";
    write_content += `   WaitMs(${time1})\n`;
    write_content += `   Lin(${grip_name},100,-1,0,2,0,0,-200,0,0,0)\n`;
    write_content += "   goto laminar_separator_2\n";
    write_content += "end\n"


    write_content += `Lin(${grip_name},100,-1,0,2,0,0,-200,0,0,0)\n`;

    write_content += `PTP(${transitionpoint_name1},100,-1,0)\n`

    // 回原点
    write_content = write_content
    +"PTP(pHome,100,-1,0)\n"

    var filename = `${DIR_USER}de${prefix}.lua`;
    console.log("filename = " + filename);
    //console.log("write_content:\n" + write_content);
    file_handle.write(filename, write_content);
    file_handle.chmod(filename, 0o777);

    sqliteDB.close(); 

    return SUCCESS;
}

//function generate_palletizing_depalletizing_program(luaname, flag) {
/*
    flag: 0 生成码垛程序， 1 生成拆垛程序
*/
function generate_palletizing_depalletizing_program(luaname, flag, left_formula_name, right_formula_name) {
    var filename = `${DIR_USER}${luaname}.lua`;
    var sql = "";
    var data = "";
    var i = 0;

    /*
        其中2的含义，0代表左工位，1代表右工位，
        其中10的含义，代表不同模式，最多10种模式，"a","b","c"...
        数值代表每个模式下最大偏移层数，0代表只有一层，不用偏移，1代表有两层，需要偏移一层，以此类推
    */
    /*
    var offset_layer_num = new Array();;
    for(i=0;i<2;i++){        //一维长度为2  
        offset_layer_num[i] = new Array();
        for(var j=0;j<10;j++){    //二维长度为10
            offset_layer_num[i][j] = 0;
       }
    }
    */
    var array = [];
    var UDP_IP = "";
    var UDP_port = 2021;
    var UDP_period = 2;

    // 如果左工位有配方名称，获取左工位配方相关数据
    var left_palletizing_name = "";
    var left_box_height = "";
    var left_layers = 0;
    var left_sequence = "";
    // var left_a_pattern = "";
    // var left_b_pattern = "";
    var left_lifting_shart_enable = 0;
    var left_laminar_separator_enable = 0;
    var left_laminar_separator_point_prefix = "";
    var left_delaminar_separator_point_prefix = "";
    if (left_formula_name != "") {
        var palletizing_db = DIR_PALLETIZING + left_formula_name + "/palletizing.db";
        var sqliteDB = new sqlite.SqliteDB(palletizing_db);

        sql = "select * from palletizing_name;";
        data = sqliteDB.queryget(sql);
        left_palletizing_name = data.name;
        
        sql = "select * from box_cfg;";
        data = sqliteDB.queryget(sql);
        left_box_height = data.height;
        
        sql = "select * from pattern_sequence_cfg;";
        data = sqliteDB.queryget(sql);
        left_layers = data.layers;
        left_sequence = data.sequence;
        /*
        var array = left_sequence.split(","); 
        for (i = 0; i < left_layers; i++) {
            if (array[i] == "A") {
                offset_layer_num[0][0] = i;
            }
            if (array[i] == "B") {
                offset_layer_num[0][1] = i;
            }
        }
        left_a_pattern = data.left_pattern_a;
        left_b_pattern = data.left_pattern_b; 
        */

        sql = "select * from advanced_cfg;";
        data = sqliteDB.queryget(sql);
        left_lifting_shart_enable = data.lifting_shart_enable;
        /* TODO: 修改为通用配置 */
        UDP_IP = data.UDP_IP;
        UDP_port = data.UDP_port;
        UDP_period = data.UDP_period;

        sql = "select * from laminar_separator;";
        data = sqliteDB.queryget(sql);
        left_laminar_separator_enable = data.enable;
        
        sqliteDB.close(); 
    }

    // 如果右工位有配方名称，获取右工位配方相关数据
    var right_palletizing_name = "";
    var right_box_height = "";
    var right_layers = 0;
    var right_sequence = "";
    //var right_a_pattern = "";
    //var right_b_pattern = "";
    var right_lifting_shart_enable = 0;
    var right_laminar_separator_enable = 0;
    var right_laminar_separator_point_prefix = "";
    var right_delaminar_separator_point_prefix = "";
    if (right_formula_name != "") {
        var palletizing_db = DIR_PALLETIZING + right_formula_name + "/palletizing.db";
        var sqliteDB = new sqlite.SqliteDB(palletizing_db);

        sql = "select * from palletizing_name;";
        data = sqliteDB.queryget(sql);
        right_palletizing_name = data.name;
        
        sql = "select * from box_cfg;";
        data = sqliteDB.queryget(sql);
        right_box_height = data.height;
        
        sql = "select * from pattern_sequence_cfg;";
        data = sqliteDB.queryget(sql);
        right_layers = data.layers;
        right_sequence = data.sequence;   
        /*
        array = right_sequence.split(","); 
        for (i = 0; i < right_layers; i++) {
            if (array[i] == "A") {
                offset_layer_num[1][0] = i;
            }
            if (array[i] == "B") {
                offset_layer_num[1][1] = i;
            }
        }
        right_a_pattern = data.right_pattern_a;
        right_b_pattern = data.right_pattern_b; 
        */

        sql = "select * from advanced_cfg;";
        data = sqliteDB.queryget(sql);
        right_lifting_shart_enable = data.lifting_shart_enable;

        sql = "select * from laminar_separator;";
        data = sqliteDB.queryget(sql);
        right_laminar_separator_enable = data.enable;
        
        sqliteDB.close(); 
    }

    // 码垛模式: 生成左，右工位模式程序
    if (flag == 0) {
        if (left_formula_name != "") {
            var palletizing_db = DIR_PALLETIZING + left_formula_name + "/palletizing.db";
            if (generate_per_pattern_file(palletizing_db, "left") == FAIL) {              

                return FAIL;
            }
            /*
            // 依此获取左工位模式 A 中点位，生成子程序 
            if (generate_per_pattern_file(palletizing_db, left_a_pattern, "left_a", offset_layer_num[0][0]) == FAIL) {              

                return FAIL;
            }
            // 依此获取左工位模式 B 中点位，生成子程序
            if (generate_per_pattern_file(palletizing_db, left_b_pattern, "left_b", offset_layer_num[0][1]) == FAIL) {

                return FAIL;
            }
            */
            // 左隔板开启，生成左隔板码垛程序
            if (left_laminar_separator_enable == 1) {
                left_laminar_separator_point_prefix = left_palletizing_name + "laminar_separator_left";
                if (generate_per_laminar_separator_file(palletizing_db, left_laminar_separator_point_prefix) == FAIL) {

                    return FAIL;
                }
            }
        }
        if (right_formula_name != "") {
            var palletizing_db = DIR_PALLETIZING + right_formula_name + "/palletizing.db";
            if (generate_per_pattern_file(palletizing_db, "right") == FAIL) {              

                return FAIL;
            }
            /*
            // 依此获取右工位模式 A 中点位，生成子程序
            if (generate_per_pattern_file(palletizing_db, right_a_pattern, "right_a", offset_layer_num[1][0]) == FAIL) {

                return FAIL;
            }
            // 依此获取右工位模式 B 中点位，生成子程序
            if (generate_per_pattern_file(palletizing_db, right_b_pattern, "right_b", offset_layer_num[1][1]) == FAIL) {

                return FAIL;
            }
            */
            // 右隔板开启，生成右隔板码垛程序
            if (right_laminar_separator_enable == 1) {
                right_laminar_separator_point_prefix = right_palletizing_name + "laminar_separator_right";
                if (generate_per_laminar_separator_file(palletizing_db, right_laminar_separator_point_prefix) == FAIL) {

                    return FAIL;
                }
            }
        }
    // 拆垛模式: 生成左，右工位模式程序
    } else {
        if (left_formula_name != "") {
            var palletizing_db = DIR_PALLETIZING + left_formula_name + "/palletizing.db";
            // /* 依此获取左工位模式 A 中点位，生成子程序 */
            // if (generate_per_depattern_file(palletizing_db, left_a_pattern, "left_a", offset_layer_num[0][0]) == FAIL) {

            //     return FAIL;
            // }
            // /* 依此获取左工位模式 B 中点位，生成子程序 */
            // if (generate_per_depattern_file(palletizing_db, left_b_pattern, "left_b", offset_layer_num[0][1]) == FAIL) {

            //     return FAIL;
            // }
            if (generate_per_depattern_file(palletizing_db, "left") == FAIL) {              

                return FAIL;
            }
            // 左隔板开启，生成左隔板拆垛程序
            if (left_laminar_separator_enable == 1) {
                left_delaminar_separator_point_prefix = left_palletizing_name + "laminar_separator_left";
                if (generate_per_delaminar_separator_file(palletizing_db, left_delaminar_separator_point_prefix) == FAIL) {

                    return FAIL;
                }
            }
        }
        if (right_formula_name != "") {
            var palletizing_db = DIR_PALLETIZING + right_formula_name + "/palletizing.db";
            // /* 依此获取右工位模式 A 中点位，生成子程序 */
            // if (generate_per_depattern_file(palletizing_db, right_a_pattern, "right_a", offset_layer_num[1][0]) == FAIL) {

            //     return FAIL;
            // }
            // /* 依此获取右工位模式 B 中点位，生成子程序 */
            // if (generate_per_depattern_file(palletizing_db, right_b_pattern, "right_b", offset_layer_num[1][1]) == FAIL) {

            //     return FAIL;
            // }
            if (generate_per_depattern_file(palletizing_db, "right") == FAIL) {              

                return FAIL;
            }
            // 右隔板开启，生成右隔板拆垛程序
            if (right_laminar_separator_enable == 1) {
                right_delaminar_separator_point_prefix = right_palletizing_name + "laminar_separator_right";
                if (generate_per_delaminar_separator_file(palletizing_db, right_delaminar_separator_point_prefix) == FAIL) {

                    return FAIL;
                }
            }
        }
    }                                        
    
    // 生成子程序的序号，不能重复
    var j = 0;

    var write_content = 
    "--** main **--\n" +
    "--** UDP **--\n" +
    "ExtDevSetUDPComParam(\"" + UDP_IP +"\", " + UDP_port + ", " + UDP_period +", 50, 2, 100, 1, 50, 20)\n" +
    "ExtDevLoadUDPDriver()\n"
    if (right_lifting_shart_enable == 1 || left_lifting_shart_enable == 1) {
        write_content += "WaitMs(500)\n"
        + "ExtAxisServoOn(1,1)\n"
        + "WaitMs(500)\n"
        + "SetAuxDO(7,1,0,0)\n"
    }
    //获取程序运行状态
    //0：代表程序动作第一次执行或者用户"接续界面"点击重新开始, 初始化程序中所有的系统变量
    //1：代表程序运行中意外停止或者手动停止，动作未完成（可“接续”）, 用户"接续界面"点击接续, 保持程序中当前的所有的系统变量
    write_content += "status = GetSysVarValue(s_var_20)\n"
    + "if status == 0 then\n"
    + "   SetSysVarValue(s_var_1, 0)\n"
    + "   SetSysVarValue(s_var_2, 0)\n"
    + "   SetSysVarValue(s_var_3, 0)\n"
    + "   SetSysVarValue(s_var_4, 0)\n"
    + "   SetSysVarValue(s_var_5, 0)\n"
    + "   SetSysVarValue(s_var_6, 0)\n"
    + "   SetSysVarValue(s_var_7, 0)\n"
    + "   SetSysVarValue(s_var_9, 0)\n"
    + "   SetSysVarValue(s_var_10, 0)\n"
    + "end\n"
    //s_var_9： 右工位层数， s_var_10： 右工位工件编号

    write_content += "WaitMs(100)\n"
    + "product_count = GetSysVarValue(s_var_1)\n"           //生产个数
    + "total_time = GetSysVarValue(s_var_2)\n"              //生产总时间
    + "single_time = GetSysVarValue(s_var_3)\n"             //单个工件生产时间
    + "layer_index = GetSysVarValue(s_var_4)\n"             //层数
    + "box_index = GetSysVarValue(s_var_5)\n"               //工件编号
    + "left_product_count = GetSysVarValue(s_var_6)\n"
    + "right_product_count = GetSysVarValue(s_var_7)\n"
    + "pallet = GetSysVarValue(s_var_8)\n"                  //当前工作在左托盘，还是右托盘，1:左托盘，2:右托盘
    + "right_layer_index = GetSysVarValue(s_var_9)\n"       //右工位层数
    + "right_box_index = GetSysVarValue(s_var_10)\n"        //右工位工件编号
    + "SetAuxDO(0,0,0,0)\n"
    + "SetAuxDO(1,0,0,0)\n"
    + "SetAuxDO(2,1,0,0)\n"
    + "SetAuxDO(3,0,0,0)\n"
    + "SetAuxDO(4,0,0,0)\n"
    + "j1,j2,j3,j4,j5,j6 = GetActualJointPosDegree()\n"
    + "x,y,z,a,b,c = GetActualTCPPose()\n"
    + "toolNum = GetActualTCPNum()\n"
    if (left_formula_name != "") {
        write_content += "if pallet == 1 then\n"
        + ` MoveL(j1,j2,j3,j4,j5,j6,x,y,z,a,b,c,toolNum,0,100,180,100,0,0.000,0.000,0.000,0.000,0,1,0,0,${left_box_height},0,0,0)\n`
        + ` PointsOffsetEnable(0,0,0,` + left_box_height*(left_layers-1) + `,0,0,0)\n`
        + ` PTP(${left_palletizing_name}lefttransitionpoint,100,-1,0)\n`
        + " PointsOffsetDisable()\n"
        + "end\n"
    }
    if (right_formula_name != "") {
        write_content += "if pallet == 2 then\n"
        + ` MoveL(j1,j2,j3,j4,j5,j6,x,y,z,a,b,c,toolNum,0,100,180,100,0,0.000,0.000,0.000,0.000,0,1,0,0,${right_box_height},0,0,0)\n`
        + ` PointsOffsetEnable(0,0,0,` + right_box_height*(right_layers-1) + `,0,0,0)\n`
        + ` PTP(${right_palletizing_name}righttransitionpoint,100,-1,0)\n`
        + " PointsOffsetDisable()\n"
        + "end\n"
    }
    write_content += "PTP(pHome,100,-1,0)\n"
    if (right_lifting_shart_enable == 1 || left_lifting_shart_enable == 1) {
        write_content += "EXT_AXIS_PTP(0,zero,30)\n"
    }
    write_content += "SetAuxDO(0,1,0,0)\n"
    + "SetAuxDO(1,0,0,0)\n"
    + "SetAuxDO(2,0,0,0)\n"
    + "SetAuxDO(22,0,0,0)\n"
    + "SetAuxDO(23,0,0,0)\n"
    + "if layer_index == 0 then\n" // 码垛过程中运行中断，用户点击重新开始运行，复位后需要把托盘置为初始状态
    + " pallet = 0\n"
    + " SetSysVarValue(s_var_8, pallet)\n"
    + "end\n"
    + "\n"
    + "while (1) do\n";
    
    /** 左工位启动,程序生成 */
    if (left_formula_name != "") {
        array = left_sequence.split(","); 
        
        write_content = write_content 
        + " id, flag = GetAuxDIChangeSign(0,1)\n"
        + " if id == 0 and flag == 1 then\n"
        + "     SetAuxDO(22,1,0,0)\n"
        + "     leftsenser1 = GetAuxDI(2,0)\n"
        + "     leftsenser2 = GetAuxDI(3,0)\n"
        + "     leftsenser3 = GetAuxDI(4,0)\n";
        if (palletizing_mode == 1) {
            write_content +=  "     if (leftsenser1 == 1 and leftsenser2 == 1 and leftsenser3 == 1) and (pallet ~= 2) then\n"
        } else {
            write_content +=  "     if leftsenser2 == 1 and (pallet ~= 2) then\n"
        }
        write_content = write_content 
        + "         pallet = 1\n"
        + "         status = 1\n"
        + "         SetSysVarValue(s_var_8, pallet)\n"
        + "         SetSysVarValue(s_var_20, status)\n"
        + "         SetAuxDO(0,0,0,0)\n"
        + "         SetAuxDO(1,1,0,0)\n"
        + "         SetAuxDO(2,0,0,0)\n";
        // 回原点 
        write_content += "          PTP(pHome,100,-1,0)\n";
        write_content += "          if layer_index == 0 then\n";
        if (flag == 0) {
            write_content += "              layer_index = 1\n";
        } else {
            write_content += `              layer_index = ${left_layers}\n`;
        }
         write_content = write_content
         + "               SetSysVarValue(s_var_4, layer_index)\n"
         + "          end\n";
         if (array.length == left_layers) {
             for (i = 0; i < left_layers; i++) {
                if (flag == 0) {
                    write_content += "          layer = " + (i+1) + "\n";
                    write_content += "          if layer_index == layer then\n";
                             
                    j = j + 1;             
                    write_content += "              NewDofile(\"/usr/local/etc/controller/lua/" + left_palletizing_name + "left_pattern_" + array[i] + ".lua\", 1, " + j + ")\n              DofileEnd()\n";
                    // if (array[i] == "A") {     
                    //     j = j + 1;             
                    //     write_content += "              NewDofile(\"/usr/local/etc/controller/lua/" + left_palletizing_name +"left_pattern_a.lua\", 1, " + (j) + ")\n             DofileEnd()\n";
                    // }
                    // if (array[i] == "B") {
                    //     j = j + 1;                      
                    //     write_content += "              NewDofile(\"/usr/local/etc/controller/lua/" + left_palletizing_name +"left_pattern_b.lua\", 1, " + (j) + ")\n             DofileEnd()\n";
                    // }
                    // 左隔板开启，生成左隔板码垛程序
                    if (left_laminar_separator_enable == 1) {
                        j = j + 1;   
                        write_content += "              NewDofile(\"/usr/local/etc/controller/lua/" + left_laminar_separator_point_prefix + ".lua\", 1, " + (j) + ")\n              DofileEnd()\n";
                    }
                    write_content = write_content
                    +"              layer_index = GetSysVarValue(s_var_4) + 1\n"
                    +"              SetSysVarValue(s_var_4, layer_index)\n"
                    +"              box_index = 0\n"
                    +"              SetSysVarValue(s_var_5, box_index)\n"
                    +"          end\n"
                } else {
                    write_content += "          layer = " + (left_layers-i) + "\n";
                    write_content += "          if layer_index == layer then\n";

                    // 左隔板开启，生成左隔板拆垛程序
                    if (left_laminar_separator_enable == 1) {
                        j = j + 1;   
                        write_content += "              NewDofile(\"/usr/local/etc/controller/lua/" + "de" + left_delaminar_separator_point_prefix + ".lua\", 1, " + (j) + ")\n              DofileEnd()\n";
                    }
                    
                    j = j + 1;             
                    write_content += "              NewDofile(\"/usr/local/etc/controller/lua/" + left_palletizing_name + "left_depattern_" + array[left_layers-i-1] + ".lua\", 1, " + j + ")\n              DofileEnd()\n";

                    // if (array[left_layers-i-1] == "A") { 
                    //     j = j + 1;                       
                    //     write_content += "              NewDofile(\"/usr/local/etc/controller/lua/" + left_palletizing_name +"left_depattern_a.lua\", 1, " + (j) + ")\n               DofileEnd()\n";
                    // }
                    // if (array[left_layers-i-1] == "B") {   
                    //     j = j + 1;                     
                    //     write_content += "              NewDofile(\"/usr/local/etc/controller/lua/" + left_palletizing_name +"left_depattern_b.lua\", 1, " + (j) + ")\n               DofileEnd()\n";
                    // }
                    write_content = write_content
                    +"              layer_index = GetSysVarValue(s_var_4) - 1\n"
                    +"              SetSysVarValue(s_var_4, layer_index)\n"
                    +"              box_index = 0\n"
                    +"              SetSysVarValue(s_var_5, box_index)\n"
                    +"          end\n"
                }
             }
         }
        write_content = write_content
        +"          layer_index = 0\n"
        +"          box_index = 0\n"
        +"          pallet = 0\n"
        +"          status = 0\n"
        +"          SetSysVarValue(s_var_4, layer_index)\n"
        +"          SetSysVarValue(s_var_5, box_index)\n"
        +"          SetSysVarValue(s_var_8, pallet)\n"
        +"          SetSysVarValue(s_var_20, status)\n"
        +"          SetAuxDO(0,1,0,0)\n"
        +"          SetAuxDO(1,0,0,0)\n"
        +"          SetAuxDO(2,0,0,0)\n"
        +"          SetAuxDO(3,1,0,0)\n"
        +"          sleep_ms(2000)\n"
        +"          SetAuxDO(3,0,0,0)\n"    
        +"      else\n" // 蜂鸣器报警3声
        +"          sensererror = 0\n" 
        +"          while sensererror < 3 do\n"
        +"              sleep_ms(1000)\n"
        +"              SetAuxDO(3,1,0,0)\n"
        +"              sleep_ms(1000)\n"
        +"              SetAuxDO(3,0,0,0)\n"
        +"              sensererror = sensererror + 1\n"
        +"          end\n"
        +"          if pallet == 2 then\n" //原本右工位码垛未完成, 用户点击“接续”后，误触了左工位启动按钮
        +"              Pause(12)\n"
        +"          else\n" //感应器未到位
        +"              Pause(10)\n"
        +"          end\n"
        +"          PTP(pHome,100,-1,0)\n"
        +"      end\n"
        +"      SetAuxDO(22,0,0,0)\n"
        +"  end\n";
    }

    /** 右工位启动,程序生成 */
    if (right_formula_name != "") {
        array = right_sequence.split(","); 
        
        write_content = write_content 
        + "    id, flag = GetAuxDIChangeSign(1,1)\n"
        + "    if id == 1 and flag == 1 then\n"
        + "        SetAuxDO(23,1,0,0)\n"
        + "        rightsenser1 = GetAuxDI(5,0)\n"
        + "        rightsenser2 = GetAuxDI(6,0)\n"
        + "        rightsenser3 = GetAuxDI(7,0)\n";
        if (palletizing_mode == 1) {
            write_content +=  "        if (rightsenser1 == 1 and rightsenser2 == 1 and rightsenser3 == 1) and (pallet ~= 1) then\n"
        } else {
            write_content +=  "        if rightsenser1 == 1 and (pallet ~= 1) then\n"
        }
        write_content = write_content 
        + "            pallet = 2\n"
        + "            status = 1\n"
        + "            SetSysVarValue(s_var_8, pallet)\n"
        + "            SetSysVarValue(s_var_20, status)\n"
        + "            SetAuxDO(0,0,0,0)\n"
        + "            SetAuxDO(1,1,0,0)\n"
        + "            SetAuxDO(2,0,0,0)\n";
        // 回原点 
        write_content += "            PTP(pHome,100,-1,0)\n";
        write_content += "            if layer_index == 0 then\n";
        if (flag == 0) {
            write_content += "                layer_index = 1\n";
        } else {
            write_content += `                layer_index = ${right_layers}\n`;
        }
         write_content = write_content
         + "                SetSysVarValue(s_var_4, layer_index)\n"
         + "            end\n";
         if (array.length == right_layers) {
             for (i = 0; i < right_layers; i++) {
                if (flag == 0) {
                    write_content += "            layer = " + (i+1) + "\n";
                    write_content += "            if layer_index == layer then\n";
                    
                    j = j + 1;
                    if (left_formula_name != "") { 
                        j = j + left_layers;
                    }
                     write_content += "              NewDofile(\"/usr/local/etc/controller/lua/" + right_palletizing_name +"right_pattern_" + array[i] + ".lua\", 1, " + j + ")\n               DofileEnd()\n";

                    // if (array[i] == "A") { 
                    //     j = j + 1;
                    //     if (left_formula_name != "") { 
                    //         write_content += "              NewDofile(\"/usr/local/etc/controller/lua/" + right_palletizing_name +"right_pattern_a.lua\", 1, " + (j) + ")\n               DofileEnd()\n";
                    //     } else {                
                    //         write_content += "              NewDofile(\"/usr/local/etc/controller/lua/" + right_palletizing_name +"right_pattern_a.lua\", 1, " + (j) + ")\n               DofileEnd()\n";
                    //     }
                    // }
                    // if (array[i] == "B") {
                    //     j = j + 1;                
                    //     if (left_formula_name != "") { 
                    //         write_content += "              NewDofile(\"/usr/local/etc/controller/lua/" + right_palletizing_name +"right_pattern_b.lua\", 1, " + (j) + ")\n               DofileEnd()\n";
                    //     } else {                
                    //         write_content += "              NewDofile(\"/usr/local/etc/controller/lua/" + right_palletizing_name +"right_pattern_b.lua\", 1, " + (j) + ")\n               DofileEnd()\n";
                    //     }
                    // }
                    // 右隔板开启，生成右隔板码垛程序
                    if (right_laminar_separator_enable == 1) {
                        j = j + 1;   
                        write_content += "              NewDofile(\"/usr/local/etc/controller/lua/" + right_laminar_separator_point_prefix + ".lua\", 1, " + (j) + ")\n              DofileEnd()\n";
                    }
                    write_content = write_content
                    +"              layer_index = GetSysVarValue(s_var_4) + 1\n"
                    +"              SetSysVarValue(s_var_4, layer_index)\n"
                    +"              box_index = 0\n"
                    +"              SetSysVarValue(s_var_5, box_index)\n"
                    +"            end\n"
                } else {
                    write_content += "            layer = " + (right_layers-i) + "\n";
                    write_content += "            if layer_index == layer then\n";
                    // 右隔板开启，生成右隔板拆垛程序
                    if (right_laminar_separator_enable == 1) {
                        j = j + 1;   
                        write_content += "              NewDofile(\"/usr/local/etc/controller/lua/" + "de" + right_delaminar_separator_point_prefix + ".lua\", 1, " + (j) + ")\n              DofileEnd()\n";
                    }

                    j = j + 1;
                    if (left_formula_name != "") { 
                        j = j + left_layers;
                    }
                    write_content += "              NewDofile(\"/usr/local/etc/controller/lua/" + right_palletizing_name +"right_depattern_" + array[right_layers-i-1] + ".lua\", 1, " + j + ")\n                DofileEnd()\n";

                    // if (array[right_layers-i-1] == "A") {   
                    //     j = j + 1;                 
                    //     if (left_formula_name != "") { 
                    //         write_content += "              NewDofile(\"/usr/local/etc/controller/lua/" + right_palletizing_name +"right_depattern_a.lua\", 1, " + (j) + ")\n               DofileEnd()\n";
                    //     } else {                
                    //         write_content += "              NewDofile(\"/usr/local/etc/controller/lua/" + right_palletizing_name +"right_depattern_a.lua\", 1, " + (j) + ")\n               DofileEnd()\n";
                    //     }
                    // }
                    // if (array[right_layers-i-1] == "B") {
                    //     j = j + 1;                     
                    //      if (left_formula_name != "") { 
                    //         write_content += "              NewDofile(\"/usr/local/etc/controller/lua/" + right_palletizing_name +"right_depattern_b.lua\", 1, " + (j) + ")\n               DofileEnd()\n";
                    //     } else {                
                    //         write_content += "              NewDofile(\"/usr/local/etc/controller/lua/" + right_palletizing_name +"right_depattern_b.lua\", 1, " + (j) + ")\n               DofileEnd()\n";
                    //     }
                    // }
                    write_content = write_content
                    +"                layer_index = GetSysVarValue(s_var_4) - 1\n"
                    +"                SetSysVarValue(s_var_4, layer_index)\n"
                    +"                box_index = 0\n"
                    +"                SetSysVarValue(s_var_5, box_index)\n"
                    +"            end\n"
                }
             }
         }
        write_content = write_content
        +"            layer_index = 0\n"
        +"            box_index = 0\n"
        +"            pallet = 0\n"
        +"            status = 0\n"
        +"            SetSysVarValue(s_var_4, layer_index)\n"
        +"            SetSysVarValue(s_var_5, box_index)\n"
        +"            SetSysVarValue(s_var_8, pallet)\n"
        +"            SetSysVarValue(s_var_20, status)\n"
        +"            SetAuxDO(0,1,0,0)\n"
        +"            SetAuxDO(1,0,0,0)\n"
        +"            SetAuxDO(2,0,0,0)\n"
        +"            SetAuxDO(3,1,0,0)\n"
        +"            sleep_ms(2000)\n"
        +"            SetAuxDO(3,0,0,0)\n"    
        +"        else\n" // 蜂鸣器报警3声
        +"            sensererror = 0\n" 
        +"            while sensererror < 3 do\n"
        +"                sleep_ms(1000)\n"
        +"                SetAuxDO(3,1,0,0)\n"
        +"                sleep_ms(1000)\n"
        +"                SetAuxDO(3,0,0,0)\n"
        +"                sensererror = sensererror + 1\n"
        +"            end\n"
        +"            if pallet == 1 then\n" //原本左工位码垛未完成, 用户点击“接续”后，误触了右工位启动按钮
        +"                Pause(13)\n"
        +"            else\n" //感应器未到位
        +"                Pause(11)\n"
        +"            end\n"
        +"            PTP(pHome,100,-1,0)\n"
        +"        end\n"
        +"        SetAuxDO(23,0,0,0)\n"
        +"    end\n";
    }
    write_content += "end\n";
       
    //console.log("write_content:\n" + write_content);
    console.log("filename: " + filename);
    file_handle.write(filename, write_content);
    file_handle.chmod(filename, 0o777);
}


function act() {
    var socket_cmd = new Socket_Cmd();
    var socket_status = new Socket_Status();
    var socket_file = new Socket_File();
    var event_socket = new events.EventEmitter();
    
    this.handler = function(json_data, res) {
        console.log("json_data = " + json_data);
        var cmd = json_data.cmd;
        console.log("cmd = " + cmd);
        var data_json = json_data.data;
        var response_data = {}
        var response_status = 200;
        var recv_data = "";
        var obj = [];
        var cmdStr = "";
        var palletizing_dir = "";
        var palletizing_db = "";
        var new_palletizing_dir = "";
        var old_palletizing_dir = "";
        var new_palletizing_db = "";
        var i;
        var j;
        
        switch (cmd) {
            case 'frcap_palletizer_create_formula':
                //判断是否有同名的
                var files = [];
                var pa = fs.readdirSync(DIR_PALLETIZING);
                pa.forEach(function (ele, index) {
                    if (ele == data_json.name) {
                        response_status = 403;
                        //break;
                    }
                });
                if (response_status == 403) {
                    // 1:"已有同名配方"
                    event_socket.emit('response', res, response_status, "1");
                    break;
                }

                palletizing_dir = "'" + DIR_PALLETIZING + data_json.name + "'";
                cmdStr = "mkdir " + palletizing_dir;
                execSync(cmdStr);
                palletizing_db = DIR_PALLETIZING + data_json.name + "/palletizing.db";    
                cmdStr = "cp -f " + DB_PALLETIZING_INIT + " " + "'" + palletizing_db  + "'";
                execSync(cmdStr);
                // exec(cmdStr, function(err,stdout,stderr){
                //     if (err) {               
                //         console.log('get api error:'+stderr);                
                //     } else {               
                //         console.log(stdout);
                        
            
                //     }               
                // });
                var sql = "update palletizing_name set name='" + data_json.name + "';";
                //console.log("sql = " + sql);
                SqliteDB_exec(palletizing_db, sql);
                event_socket.emit('response', res, response_status, "success");               
                break;
            case 'frcap_palletizer_rename_formula':
                //判断是否有同名的
                var files = [];
                var pa = fs.readdirSync(DIR_PALLETIZING);
                pa.forEach(function (ele, index) {
                    if (ele == data_json.new_name) {
                        response_status = 403;
                        //break;
                    }
                });
                if (response_status == 403) {
                    event_socket.emit('response', res, response_status, "1");
                    break;
                }

                new_palletizing_dir = "'" + DIR_PALLETIZING + data_json.new_name + "'";
                old_palletizing_dir = "'" + DIR_PALLETIZING + data_json.old_name + "'";    
                cmdStr = "mv " + old_palletizing_dir + " " + new_palletizing_dir;
                execSync(cmdStr);
                var sql = "update palletizing_name set name='" + data_json.new_name + "';";
                new_palletizing_db = DIR_PALLETIZING + data_json.new_name + "/palletizing.db";
                SqliteDB_exec(new_palletizing_db, sql);
                event_socket.emit('response', res, response_status, "success");               
                break;
            case 'frcap_palletizer_copy_formula':
                //判断是否有同名的
                var files = [];
                var pa = fs.readdirSync(DIR_PALLETIZING);
                pa.forEach(function (ele, index) {
                    if (ele == data_json.new_name) {
                        response_status = 403;
                        //break;
                    }
                });
                if (response_status == 403) {
                    event_socket.emit('response', res, response_status, "1");
                    break;
                }

                new_palletizing_dir = "'" + DIR_PALLETIZING + data_json.new_name + "'";
                old_palletizing_dir = "'" + DIR_PALLETIZING + data_json.old_name + "'";     
                cmdStr = "cp -rf " + old_palletizing_dir + " " + new_palletizing_dir;
                execSync(cmdStr);

                var sql = "update palletizing_name set name='" + data_json.new_name + "';";
                new_palletizing_db = DIR_PALLETIZING + data_json.new_name + "/palletizing.db";
                SqliteDB_exec(new_palletizing_db, sql);

                event_socket.emit('response', res, response_status, "success");               
                break;
            case 'frcap_palletizer_delete_formula':
                palletizing_dir = "'" + DIR_PALLETIZING + data_json.name + "'";   
                cmdStr = "rm -rf " + palletizing_dir;
                execSync(cmdStr);       
                event_socket.emit('response', res, response_status, "success");               
                break;
            case 'clear_palletizing_info': 
                socket_cmd.send("511", "SetSysVarValue(1,0)");
                socket_cmd.send("511", "SetSysVarValue(2,0)");
                socket_cmd.send("511", "SetSysVarValue(3,0)");
                socket_cmd.send("511", "SetSysVarValue(4,0)");
                socket_cmd.send("511", "SetSysVarValue(5,0)");
                socket_cmd.send("511", "SetSysVarValue(6,0)");
                socket_cmd.send("511", "SetSysVarValue(7,0)");
                socket_cmd.send("511", "SetSysVarValue(8,0)");
                socket_cmd.send("511", "SetSysVarValue(20,0)");
                socket_cmd.recv().then((recv_data)=>{ 
                    //console.log("recv_data = " + recv_data);                           
                    event_socket.emit('response', res, response_status, "success");               
                }).catch((err)=>{
                    console.log(err);
                })
                break;
            case 'frcap_palletizer_config_box':
                palletizing_db = DIR_PALLETIZING + data_json.formula_name + "/palletizing.db";

                for (i = 0; i < data_json.boxs.length; i++) {
                    obj[0] = 1;
                    obj[1] = data_json.boxs[i].box_id;
                    obj[2] = data_json.boxs[i].name;
                    obj[3] = data_json.boxs[i].length;
                    obj[4] = data_json.boxs[i].width;
                    obj[5] = data_json.boxs[i].height;
                    obj[6] = data_json.boxs[i].payload;
                    obj[7] = data_json.boxs[i].grip_point;
                    obj[8] = data_json.boxs[i].IO;
                    obj[9] = data_json.boxs[i].tag_enable;
                    obj[10] = data_json.boxs[i].tag;

                    for (j = 0; j < 11; j++) {
                        if (obj[j] === "") {
                            response_status = 403; 
                        } 
                    }
                    // 如果下发的参数为空，认为该条数据不完整，不更新对应的数据库文件
                    if (response_status != 403) {
                        var sql = "insert into box_cfg values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";             
                        sqlite.exec(palletizing_db, sql, obj)
                    }
                }
                // if (response_status == 403) {
                //     event_socket.emit('response', res, response_status, "2");
                //     break;
                // }
                event_socket.emit('response', res, response_status, "success");                        
                break;
            case 'frcap_palletizer_config_box_add':
                palletizing_db = DIR_PALLETIZING + data_json.formula_name + "/palletizing.db";

                var sqliteDB = new sqlite.SqliteDB(palletizing_db);
                var sql = "select box_id from box_cfg;";
                var data = sqliteDB.queryall(sql);
                console.log("data = " + JSON.stringify(data));
                box_id = 1;
                for (i = 0; i < data.length; i++) {
                    if (data[i].box_id > box_id) {
                        box_id = data[i].box_id;
                    }
                }
                box_id = box_id + 1;
                sqliteDB.close(); 

                obj[0] = 0;
                obj[1] = box_id;
                obj[2] = "box_" + box_id;
                obj[3] = 400;
                obj[4] = 300;
                obj[5] = 300;
                obj[6] = 0.0;
                obj[7] = "";
                obj[8] = "[4,5]";
                obj[9] = 0;
                obj[10] = 1;
                var sql = "insert into box_cfg values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";             
                sqlite.exec(palletizing_db, sql, obj)
                event_socket.emit('response', res, response_status, "success");                        
                break;
            case 'frcap_palletizer_config_box_rename':
                palletizing_db = DIR_PALLETIZING + data_json.formula_name + "/palletizing.db";
                obj[0] = data_json.name;
                obj[1] = data_json.box_id;
                var sql = "update box_cfg set name=? where box_id=?;";             
                sqlite.exec(palletizing_db, sql, obj)
                event_socket.emit('response', res, response_status, "success");                        
                break;
            case 'frcap_palletizer_config_box_delete':
                palletizing_db = DIR_PALLETIZING + data_json.formula_name + "/palletizing.db";
                obj[0] = data_json.box_id;
                var sql = "delete from box_cfg where box_id=?;";             
                sqlite.exec(palletizing_db, sql, obj)
                event_socket.emit('response', res, response_status, "success");                        
                break;
            case 'frcap_palletizer_config_box_copy':
                palletizing_db = DIR_PALLETIZING + data_json.formula_name + "/palletizing.db";

                var sqliteDB = new sqlite.SqliteDB(palletizing_db);
                var sql = "select * from box_cfg where box_id = " + data_json.box_id + ";";
                var box_info = sqliteDB.queryget(sql);

                sql = "select box_id from box_cfg;";
                var data = sqliteDB.queryall(sql);
                var box_id = 1;
                for (i = 0; i < data.length; i++) {
                    if (data[i].box_id > box_id) {
                        box_id = data[i].box_id;
                    }
                }
                box_id = box_id + 1;
                sqliteDB.close(); 

                obj[0] = box_info.flag;
                obj[1] = box_id;
                obj[2] = box_info.name + "_copy";
                obj[3] = box_info.length;
                obj[4] = box_info.width;
                obj[5] = box_info.height;
                obj[6] = box_info.payload;
                obj[7] = data_json.formula_name + "_" + box_id + "_grippoint";
                obj[8] = box_info.IO;
                obj[9] = box_info.tag_enable;
                obj[10] = box_info.tag;
                var sql = "insert into box_cfg values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";             
                sqlite.exec(palletizing_db, sql, obj)
                event_socket.emit('response', res, response_status, "success");                        
                break;
            case 'frcap_palletizer_config_pallet':
                if (data_json.front === "" || data_json.side === "" || data_json.height === "") {
                    response_status = 403;
                    event_socket.emit('response', res, response_status, "2");
                    break;
                }   
                palletizing_db = DIR_PALLETIZING + data_json.formula_name + "/palletizing.db";               
                var sql = "update pallet_cfg set flag=1, front=" + data_json.front + ", side=" + data_json.side + ", height=" + data_json.height + ";";
                sqlite.exec(palletizing_db, sql, obj);
                event_socket.emit('response', res, response_status, "success");                        
                break;
            case 'frcap_palletizer_laminar_separator':
                if (data_json.enable === "" || data_json.length === "" || data_json.width === "" || data_json.height === "") {
                    response_status = 403;
                    event_socket.emit('response', res, response_status, "2");
                    break;
                }  
                palletizing_db = DIR_PALLETIZING + data_json.formula_name + "/palletizing.db";
                var sql = "update laminar_separator set flag=1, enable=" + data_json.enable + ", length=" + data_json.length + ", width=" + data_json.width + ", height=" + data_json.height + ";";
                /* 使用 better-sqlite3 封装同步的 API 替代原本的 sqlite3 异步 API, 提高性能同时保证数据库操作的完整性 */
                sqlite.exec(palletizing_db, sql, obj);
                //SqliteDB_exec(palletizing_db, sql);
                event_socket.emit('response', res, response_status, "success");                      
                break;
            case 'frcap_palletizer_config_pattern':
                palletizing_db = DIR_PALLETIZING + data_json.formula_name + "/palletizing.db";
                console.log("data_json.patterns.length = " + data_json.patterns.length);

                for (i = 0; i < data_json.patterns.length; i++) {
                    obj[0] = 1;
                    obj[1] = data_json.patterns[i].pattern_id;
                    obj[2] = data_json.patterns[i].type;
                    obj[3] = data_json.patterns[i].name;
                    obj[4] = data_json.patterns[i].box_gap;
                    obj[5] = data_json.patterns[i].box_ids;
                    obj[6] = data_json.patterns[i].left_pattern;
                    obj[7] = data_json.patterns[i].right_pattern;
                    obj[8] = data_json.patterns[i].origin_pattern;

                    for (j = 0; j < 9; j++) {
                        if (obj[j] === "") {
                            response_status = 403; 
                        } 
                    }
                    if (response_status != 403) {
                        var sql = "insert into pattern_cfg values (?, ?, ?, ?, ?, ?, ?, ?, ?);";             
                        sqlite.exec(palletizing_db, sql, obj)
                    }
                }
                
                if (response_status == 403) {
                    event_socket.emit('response', res, response_status, "2");
                    break;
                }
                event_socket.emit('response', res, response_status, "success");                       
                break;
            case 'frcap_palletizer_config_pattern_add':
                palletizing_db = DIR_PALLETIZING + data_json.formula_name + "/palletizing.db";

                var sqliteDB = new sqlite.SqliteDB(palletizing_db);
                var sql = "select pattern_id from pattern_cfg;";
                var data = sqliteDB.queryall(sql);
                console.log("data = " + JSON.stringify(data));
                var pattern_id = 1;
                for (i = 0; i < data.length; i++) {
                    if (data[i].pattern_id > pattern_id) {
                        pattern_id = data[i].pattern_id;
                    }
                }
                pattern_id = pattern_id + 1;
                sqliteDB.close(); 

                obj[0] = 0;
                obj[1] = pattern_id;
                let config = {
                    1: 'A',
                    2: 'B',
                    3: 'C',
                    4: 'D',
                    5: 'E',
                    6: 'F',
                    7: 'G',
                    8: 'H',
                    9: 'I',
                    10: 'J'
                }
                obj[2] = config[pattern_id];
                obj[3] = "pattern_" + config[pattern_id];
                obj[4] = 0;
                obj[5] = "[]";
                obj[6] = "{\"1\": []}";
                obj[7] = "{\"1\": []}";
                obj[8] = "[]";
                var sql = "insert into pattern_cfg values (?, ?, ?, ?, ?, ?, ?, ?, ?);";             
                sqlite.exec(palletizing_db, sql, obj)
                event_socket.emit('response', res, response_status, "success");                        
                break;
            case 'frcap_palletizer_config_pattern_rename':
                palletizing_db = DIR_PALLETIZING + data_json.formula_name + "/palletizing.db";
                obj[0] = data_json.name;
                obj[1] = data_json.pattern_id;
                var sql = "update pattern_cfg set name=? where pattern_id=?;";             
                sqlite.exec(palletizing_db, sql, obj)
                event_socket.emit('response', res, response_status, "success");                        
                break;
            case 'frcap_palletizer_config_pattern_delete':
                palletizing_db = DIR_PALLETIZING + data_json.formula_name + "/palletizing.db";
                obj[0] = data_json.pattern_id;
                var sql = "delete from pattern_cfg where pattern_id=?;";             
                sqlite.exec(palletizing_db, sql, obj)
                event_socket.emit('response', res, response_status, "success");                        
                break;
            case 'frcap_palletizer_config_pattern_copy':
                palletizing_db = DIR_PALLETIZING + data_json.formula_name + "/palletizing.db";

                var sqliteDB = new sqlite.SqliteDB(palletizing_db);
                var sql = "select * from pattern_cfg where pattern_id = " + data_json.pattern_id + ";";
                var pattern_info = sqliteDB.queryget(sql);

                sql = "select pattern_id from pattern_cfg;";
                var data = sqliteDB.queryall(sql);
                var pattern_id = 1;
                for (i = 0; i < data.length; i++) {
                    if (data[i].pattern_id > pattern_id) {
                        pattern_id = data[i].pattern_id;
                    }
                }
                pattern_id = pattern_id + 1;

                console.log("pattern_id = " + pattern_id);

                obj[0] = pattern_info.flag;
                obj[1] = pattern_id;
                obj[2] = pattern_info.type;
                obj[3] = pattern_info.name + "_copy";
                obj[4] = pattern_info.box_gap;
                obj[5] = pattern_info.box_ids;
                obj[6] = pattern_info.left_pattern;
                obj[7] = pattern_info.right_pattern;
                obj[8] = pattern_info.origin_pattern;

                sqliteDB.close(); 

                var sql = "insert into pattern_cfg values (?, ?, ?, ?, ?, ?, ?, ?, ?);";             
                sqlite.exec(palletizing_db, sql, obj)
                event_socket.emit('response', res, response_status, "success");                        
                break;
            case 'frcap_palletizer_config_pattern_sequence':
                palletizing_db = DIR_PALLETIZING + data_json.formula_name + "/palletizing.db";
                
                obj[0] = 1;
                obj[1] = data_json.layers;
                obj[2] = data_json.sequence;
                var sql = "update pattern_sequence_cfg set flag=?, layers=?, sequence=?;"; 
                sqlite.exec(palletizing_db, sql, obj)     
                event_socket.emit('response', res, response_status, "success");                       
                break;
            case 'frcap_palletizer_config_device':
                palletizing_db = DIR_PALLETIZING + data_json.formula_name + "/palletizing.db";
                //console.log("palletizing_db = " + palletizing_db);
                if (data_json.x === "" || data_json.y === "" || data_json.z === "" || data_json.angle === "") {
                    response_status = 403;
                    event_socket.emit('response', res, response_status, "2");
                    break;
                }   
                var sql = "update device_cfg set flag=1, x=" + data_json.x + ", y=" + data_json.y + ", z=" + data_json.z + ", angle=" + data_json.angle + ";";
                //console.log("sql = " + sql);
                sqlite.exec(palletizing_db, sql, obj)
                /*
                var files = [];
                var pa = fs.readdirSync(DIR_PALLETIZING);
                pa.forEach(function (ele, index) {
                    palletizing_db = DIR_PALLETIZING + ele + "/palletizing.db";
                    var sql = "update device_cfg set flag=1, x=" + data_json.x + ", y=" + data_json.y + ", z=" + data_json.z + ", angle=" + data_json.angle + ";";
                    SqliteDB_exec(palletizing_db, sql);
                });     
                */       
                event_socket.emit('response', res, response_status, "success");                       
                break;
            case 'frcap_palletizer_advanced_cfg':
                palletizing_db = DIR_PALLETIZING + data_json.formula_name + "/palletizing.db";  
                obj[0] = data_json.height;
                obj[1] = data_json.x1;
                obj[2] = data_json.y1;
                obj[3] = data_json.z1;
                obj[4] = data_json.x2;
                obj[5] = data_json.y2;
                obj[6] = data_json.z2;
                obj[7] = data_json.time;
                /*
                obj[8] = data_json.smooth_enable;
                obj[9] = data_json.smooth1_time;
                obj[10] = data_json.smooth1_length;
                obj[11] = data_json.smooth2_time;
                obj[12] = data_json.smooth2_length;
                */
                obj[8] = '0';
                obj[9] = '100';
                obj[10] = '100';
                obj[11] = '100';
                obj[12] = '100';
                obj[13] = data_json.lifting_shart_enable;
                obj[14] = data_json.lifting_shart_layer;
                obj[15] = data_json.UDP_IP;
                obj[16] = data_json.UDP_port;
                obj[17] = data_json.UDP_period;
                for (i = 0; i < 18; i++) {
                    if (obj[i] === "") {
                        response_status = 403; 
                    } 
                }
                if (response_status == 403) {
                    event_socket.emit('response', res, response_status, "2");
                    break;
                }             
                var sql = "update advanced_cfg set height=?, x1=?, y1=?, z1=?, x2=?, y2=?, z2=?, time=?, smooth_enable=?, smooth1_time=?, smooth1_length=?, smooth2_time=?, smooth2_length=?, lifting_shart_enable=?, lifting_shart_layer=?, UDP_IP=?, UDP_port=?, UDP_period=?;";
                sqlite.exec(palletizing_db, sql, obj);
                event_socket.emit('response', res, response_status, "success");                        
                break;
            case 'frcap_palletizer_generate_program':
                socket_cmd.send("511", "SetSysVarValue(1,0)");
                socket_cmd.send("511", "SetSysVarValue(2,0)");
                socket_cmd.send("511", "SetSysVarValue(3,0)");
                socket_cmd.send("511", "SetSysVarValue(4,0)");
                socket_cmd.send("511", "SetSysVarValue(5,0)");
                socket_cmd.send("511", "SetSysVarValue(6,0)");
                socket_cmd.send("511", "SetSysVarValue(7,0)");
                socket_cmd.send("511", "SetSysVarValue(8,0)");
                socket_cmd.send("511", "SetSysVarValue(20,0)");
                socket_cmd.recv().then((recv_data)=>{
                    //生成码垛、拆垛程序
                        generate_palletizing_depalletizing_program(data_json.palletizing_program_name, 0, data_json.left_palletizing_name, data_json.right_palletizing_name);
                        generate_palletizing_depalletizing_program(data_json.depalletizing_program_name, 1, data_json.left_palletizing_name, data_json.right_palletizing_name);
                    /*
                    var palletizing_name = data_json.left_palletizing_name;
                    var depalletizing_name = data_json.left_depalletizing_name;
                    var flag = JSON.parse(data_json.flag);
                    var obj = [];
                    obj[0] = data_json.palletizing_name;
                    obj[1] = data_json.depalletizing_name;
                    obj[2] = data_json.flag;
                    //var sql = `update program_config set palletizing_name='${palletizing_name}', depalletizing_name='${depalletizing_name}', flag='${flag}';`
                    var sql = "update program_config set palletizing_name=?, depalletizing_name=?, flag=?;"
                    sqlite.exec(DB_PALLETIZING, sql, obj);
                    if (flag[0] == 1) {
                        generate_palletizing_depalletizing_program(data_json.palletizing_name, 0);
                    }
                    if (flag[1] == 1) {                      
                        generate_palletizing_depalletizing_program(data_json.depalletizing_name, 1);
                    }
                    */
                    var palletizing = [];
                    palletizing[0] = data_json.left_palletizing_name;
                    palletizing[1] = data_json.right_palletizing_name;

                    obj[0] = data_json.palletizing_program_name;
                    obj[1] = JSON.stringify(palletizing);
                    var sql = "insert into program_cfg values (?, ?);";
                    sqlite.exec(DB_PALLETIZING_PROGRAM, sql, obj);

                    obj[0] = data_json.depalletizing_program_name
                    obj[1] = JSON.stringify(palletizing);;
                    var sql = "insert into program_cfg values (?, ?);";
                    sqlite.exec(DB_PALLETIZING_PROGRAM, sql, obj);

                    event_socket.emit('response', res, response_status, "success");                
                }).catch((err)=>{
                    console.log(err);
                })
                break;
            default:
                response_status = 404;
                event_socket.emit('response', res, response_status, "fail");
                break;
        }  
    }
    
    event_socket.on('response', (res, response_status, response_data)=>{
        console.log("response_status = " + response_status);
        res.writeHead(response_status);
        if (response_data === "fail") {
            console.log("cmd not found")
            res.end(response_data);
        } else {
            if (Object.prototype.toString.call(response_data) === '[object Object]' || Object.prototype.toString.call(response_data) === '[object Array]') {  
                console.log("isJSON object true");
                console.log("response_data = " + JSON.stringify(response_data));
                res.end(JSON.stringify(response_data));
            } else {
                console.log("isJSON object false");
                console.log("response_data = " + response_data);
                res.end(response_data);
            }
        }
    });
}

module.exports = act;
